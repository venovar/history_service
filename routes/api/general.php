<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE');
header('Access-Control-Allow-Headers: Authorization, Content-Type');

Route::group(['prefix' => 'v1'], function ()
{
	Route::group(['prefix' => 'sprint'], function ()
	{
		Route::get('up',			'SprintController@up');
		/*Route::get('first_admin',	'SprintController@becomeFirstUserInAdmin');*/
		/*Route::get('app_path',	'SprintController@getAppPath');*/
		Route::get('update_officials',	'SprintController@updateOfficials');
		Route::get('update_device',	'SprintController@updateDevice');
		Route::get('write_devices',	'SprintController@writeDevices');
	});
	
	Route::group(['prefix' => 'log'], function ()
	{
		Route::get('show.txt',	'Controller@logShow');
		Route::get('clear',		'Controller@logClear');
	});
});