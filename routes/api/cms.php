<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE');
header('Access-Control-Allow-Headers: Authorization, Content-Type');

Route::group(['prefix' => 'v1'], function ()
{
	Route::group(['prefix' => '{locale}'], function ()
	{
		Route::group(['prefix' => 'cms'], function ()
		{
			Route::group(['prefix' => 'auth'], function ()
			{
				Route::post('signup',		'CMS\AuthController@signup');
				Route::post('login',		'CMS\AuthController@login');
				Route::post('facebook',		'CMS\AuthController@facebook');
				Route::get('logout',		'CMS\AuthController@logout');
			});
			Route::group(['prefix' => 'account'], function ()
			{
				Route::put('update', 			'CMS\AccountController@update');
				Route::get('show', 				'CMS\AccountController@show');
				Route::delete('destroy',		'CMS\AccountController@destroy');
			});
			Route::group(['prefix' => 'image'], function ()
			{
				Route::post('store', 			'CMS\ImageController@store');
				Route::delete('destroy',		'CMS\ImageController@destroy');
			});
			Route::group(['prefix' => 'image_of_note'], function ()
			{
				Route::post('store', 			'CMS\ImageOfNoteController@store');
				Route::delete('destroy',		'CMS\ImageOfNoteController@destroy');
			});
			Route::group(['prefix' => 'highlight'], function ()
			{
				Route::get('dataprovider', 		'CMS\HighlightController@dataprovider');
				Route::get('index', 			'CMS\HighlightController@index');
				Route::get('show', 				'CMS\HighlightController@show');
				Route::post('store', 			'CMS\HighlightController@store');
				Route::put('update', 			'CMS\HighlightController@update');
				Route::delete('destroy', 		'CMS\HighlightController@destroy');
				Route::group(['prefix' => 'attachment'], function ()
				{
					Route::get('check', 		'CMS\HighlightController@attachmentCheck');
				});
			});
			Route::group(['prefix' => 'video'], function ()
			{
				Route::get('index', 			'CMS\VideoController@index');
				Route::get('show', 				'CMS\VideoController@show');
				Route::post('store', 			'CMS\VideoController@store');
				Route::put('update', 			'CMS\VideoController@update');
				Route::delete('destroy', 		'CMS\VideoController@destroy');
			});
			Route::group(['prefix' => 'playlist'], function ()
			{
				Route::get('index', 			'CMS\PlaylistController@index');
				Route::get('show', 				'CMS\PlaylistController@show');
				Route::post('store', 			'CMS\PlaylistController@store');
				Route::put('update', 			'CMS\PlaylistController@update');
				Route::delete('destroy', 		'CMS\PlaylistController@destroy');
				Route::get('cover_test', 		'CMS\PlaylistController@coverTest');
				
				Route::group(['prefix' => 'video'], function ()
				{
					Route::get('index', 			'CMS\PlaylistController@videoIndex');
					Route::put('update', 			'CMS\PlaylistController@videoUpdate');
				});
			});
			Route::group(['prefix' => 'gif'], function ()
			{
				Route::get('index', 			'CMS\GifController@index');
				Route::get('show', 				'CMS\GifController@show');
				Route::post('store', 			'CMS\GifController@store');
				Route::put('update', 			'CMS\GifController@update');
				Route::delete('destroy', 		'CMS\GifController@destroy');
			});
			Route::group(['prefix' => 'curiosity'], function ()
			{
				Route::get('index', 			'CMS\CuriosityController@index');
				Route::get('show', 				'CMS\CuriosityController@show');
				Route::post('store', 			'CMS\CuriosityController@store');
				Route::put('update', 			'CMS\CuriosityController@update');
				Route::delete('destroy', 		'CMS\CuriosityController@destroy');
			});
			Route::group(['prefix' => 'article'], function ()
			{
				Route::get('index', 			'CMS\ArticleController@index');
				Route::get('show', 				'CMS\ArticleController@show');
				Route::post('store', 			'CMS\ArticleController@store');
				Route::put('update', 			'CMS\ArticleController@update');
				Route::delete('destroy', 		'CMS\ArticleController@destroy');
			});
			Route::group(['prefix' => 'serie'], function ()
			{
				Route::get('index', 			'CMS\SerieController@index');
				Route::get('show', 				'CMS\SerieController@show');
				Route::post('store', 			'CMS\SerieController@store');
				Route::put('update', 			'CMS\SerieController@update');
				Route::delete('destroy', 		'CMS\SerieController@destroy');
			});
			Route::group(['prefix' => 'institutional'], function ()
			{
				Route::get('index', 			'CMS\InstitutionalController@index');
				Route::get('show', 				'CMS\InstitutionalController@show');
				Route::post('store', 			'CMS\InstitutionalController@store');
				Route::put('update', 			'CMS\InstitutionalController@update');
				Route::delete('destroy', 		'CMS\InstitutionalController@destroy');
			});
			Route::group(['prefix' => 'user'], function ()
			{
				Route::get('index', 			'CMS\UserController@index');
				Route::get('show', 				'CMS\UserController@show');
				Route::put('update', 			'CMS\UserController@update');
				Route::delete('destroy', 		'CMS\UserController@destroy');
			});
			Route::group(['prefix' => 'app_logo'], function ()
			{
				Route::post('store',	'CMS\AppLogoController@store');
				Route::get('show',		'CMS\AppLogoController@show');
			});
			Route::group(['prefix' => 'console_test'], function ()
			{
				/*
				Route::get('image_delete',			'CMS\ImageController@forceDelete');
				Route::get('image_of_note_delete',	'CMS\ImageOfNoteController@forceDelete');
				Route::get('app_logo_delete',		'CMS\AppLogoController@forceDelete');
				Route::get('push_update',			'CMS\PushNotificationController@update');
				*/
			});
			Route::group(['prefix' => 'push_notification'], function ()
			{
				Route::get('test',	'CMS\PushNotificationController@test');
			});
		});
	});
});
		