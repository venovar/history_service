<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: PUT, GET, POST, DELETE');
header('Access-Control-Allow-Headers: Authorization, Content-Type');

Route::group(['prefix' => 'v1'], function ()
{
	Route::group(['prefix' => '{locale}'], function ()
	{
		Route::group(['prefix' => 'auth'], function ()
		{
			Route::post('login',		'Common\AuthController@login');
			Route::get('logout',		'Common\AuthController@logout');
			Route::post('facebook',		'Common\AuthController@facebook');
		});
		
		Route::group(['prefix' => 'account'], function ()
		{
			Route::put('update', 			'Common\AccountController@update');
			Route::get('show', 				'Common\AccountController@show');
			Route::delete('destroy',		'Common\AccountController@destroy');
		});
		
		Route::group(['prefix' => 'highlight'], function ()
		{
			Route::get('index', 			'Common\HighlightController@index');
			Route::get('show', 				'Common\HighlightController@show');
		});
			
		Route::group(['prefix' => 'image'], function ()
		{
			Route::post('store', 			'Common\ImageController@store');
			Route::delete('destroy',		'Common\ImageController@destroy');
		});
		
		Route::group(['prefix' => 'image_of_note'], function ()
		{
			Route::post('store', 			'Common\ImageOfNoteController@store');
			Route::delete('destroy',		'Common\ImageOfNoteController@destroy');
		});
			
		Route::group(['prefix' => 'highlight'], function ()
		{
			Route::get('index', 			'Common\HighlightController@index');
			Route::get('show', 				'Common\HighlightController@show');
		});
		Route::group(['prefix' => 'video'], function ()
		{
			Route::get('index', 			'Common\VideoController@index');
			Route::get('show', 				'Common\VideoController@show');
			Route::post('store', 			'Common\VideoController@store');
			Route::put('update', 			'Common\VideoController@update');
			Route::delete('destroy', 		'Common\VideoController@destroy');
			
			Route::group(['prefix' => 'playlist'], function ()
			{
				Route::post('add',		'Common\VideoController@playlistAdd');
				Route::delete('remove',	'Common\VideoController@playlistRemove');
			});
		});
		Route::group(['prefix' => 'playlist'], function ()
		{
			Route::get('index', 			'Common\PlaylistController@index');
			Route::get('show', 				'Common\PlaylistController@show');
			Route::post('store', 			'Common\PlaylistController@store');
			Route::put('update', 			'Common\PlaylistController@update');
			Route::delete('destroy', 		'Common\PlaylistController@destroy');
			
			Route::group(['prefix' => 'video'], function ()
			{
				Route::get('index', 			'Common\PlaylistController@videoIndex');
				Route::put('update', 			'Common\PlaylistController@videoUpdate');
			});
		});
		Route::group(['prefix' => 'gif'], function ()
		{
			Route::get('index', 			'Common\GifController@index');
			Route::get('show', 				'Common\GifController@show');
		});
		Route::group(['prefix' => 'curiosity'], function ()
		{
			Route::get('index', 			'Common\CuriosityController@index');
			Route::get('show', 				'Common\CuriosityController@show');
		});
		Route::group(['prefix' => 'article'], function ()
		{
			Route::get('index', 			'Common\ArticleController@index');
			Route::get('show', 				'Common\ArticleController@show');
		});
		Route::group(['prefix' => 'serie'], function ()
		{
			Route::get('show', 				'Common\SerieController@show');
		});
		Route::group(['prefix' => 'institutional'], function ()
		{
			Route::get('index', 			'Common\InstitutionalController@index');
			Route::get('show', 				'Common\InstitutionalController@show');
		});
		Route::group(['prefix' => 'user'], function ()
		{
			Route::get('show', 				'Common\UserController@show');
		});
		Route::group(['prefix' => 'app_logo'], function ()
		{
			Route::get('show',		'Common\AppLogoController@show');
		});
		Route::group(['prefix' => 'device'], function ()
		{
			Route::get('show',		'Common\DeviceController@show');
			Route::post('store',	'Common\DeviceController@store');
			Route::put('update',	'Common\DeviceController@update');
		});
		Route::group(['prefix' => 'push_notification'], function ()
		{
			Route::get('index',			'Common\PushNotificationController@index');
			Route::get('show',			'Common\PushNotificationController@show');
			Route::delete('destroy',	'Common\PushNotificationController@destroy');
		});
	});
});