<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;
use App\Http\Traits\ImageJobs;

use App\Playlist;
use App\Video;
use App\Image;

class PlaylistController extends Controller
{
    use ImageJobs;
    
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin']);
    }
    
    /**
     * Display a listing of the Playlist.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'author'    => 'nullable|integer|max:3',
            'highlight' => 'nullable|boolean',
            'enabled'   => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        // 1 = Oficiais
        // 2 = Usuários
        // 3 = Myself
        $playlists = Playlist::with(['image'])
            ->whereHas('author', function($query) use ($request) {
                if($request->has('author') && $request->author == 3) {
                    $query->whereId($request->user()->id);
                }
            });
        if($request->has('author') && $request->author == 1) {
            $playlists->whereOfficial(1);
        } else if($request->has('author') && $request->author == 2) {
            $playlists->whereOfficial(0);
        }
        if($request->has('enabled') && $request->enabled != null) {
            $playlists->whereEnabled($request->enabled);
        }
        if($request->has('highlight') && $request->highlight != null) {
            $playlists->whereHighlight($request->highlight);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $playlists->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $playlists->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $playlists->orderBy('score01', 'DESC');
        }
        $playlists = $playlists
            ->orderBy('highlight', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(30);
        return response()->json(compact('playlists'));
    }
    
    /**
     * Store a newly created Playlist in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'     => 'required|max:100',
            'note'      => 'required|string|max:5000',
            'enabled'   => 'boolean',
            'highlight' => 'boolean',
            'image.id'  => 'required|integer|exists:images,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $playlist = new Playlist();
        $playlist->fill($request->all());
        $playlist->author_id = $request->user()->id;
        if($playlist->save()) {
            if(filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN)) {
                $playlists = Playlist::whereHas('author', function($query) {
                        $query->whereAdmin(1);
                    })
                    ->where('id', '!=', $playlist->id)
                    ->update(['highlight' => 0]);
            }
            if($request->has('image') && !empty($request->image['id'])) {
                $image = Image::find($request->image['id']);
                if($image) {
                    $playlist->image()->save($image);
                }
            }
            $playlist->author;
            $playlist->image;
            // $this->pushNotificationStore($playlist);
            return response()->json([
                'message'   => __('messages.playlist_created'),
                'playlist' => $playlist,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Playlist.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:playlists,id,deleted_at,NULL',
            'markdown'  => 'nullable|boolean'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $playlist = Playlist::with(['author', 'image'])->find($request->id);
        
        $playlist->setMarkdown($request->markdown);
        
        return response()->json(compact('playlist'));
    }

    /**
     * Update the specified Playlist in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:playlists,id,deleted_at,NULL',
            'title'     => 'required|max:100',
            'note'      => 'required|string|max:5000',
            'enabled'   => 'boolean',
            'highlight' => 'boolean',
            'image.id'  => 'required|integer|exists:images,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $playlist = Playlist::find($request->id);
        $playlist->fill($request->all());
        if($playlist->save()) {
            if(filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN)) {
                $playlists = Playlist::whereHas('author', function($query) {
                        $query->whereAdmin(1);
                    })
                    ->where('id', '!=', $playlist->id)
                    ->update(['highlight' => 0]);
            }
            if($request->has('image') && !empty($request->image['id'])) {
                if($playlist->image) {
                    $playlist->image->imageable_id = null;
                    $playlist->image->imageable_type = null;
                    $playlist->image->save();
                }
                $image = Image::find($request->image['id']);
                if($image) {
                    $playlist->image()->save($image);
                }
            }
            $playlist->author;
            $playlist->image;
            // $this->pushNotificationStore($playlist);
            return response()->json([
                'message'   => __('messages.playlist_updated'),
                'playlist' => $playlist,
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Remove the specified Playlist from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:playlists,id,deleted_at,NULL'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $playlist = Playlist::find($request->id);
        
        if($playlist->delete()) {
            return response()->json([
                'message'   => __('messages.playlist_deleted')
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
    
    /**
     * Display a listing of the Playlist Video.
     *
     * @return \Illuminate\Http\Response
     */
    public function videoIndex(Request $request)
    {
        $rules = [
            'playlist_id'   => 'nullable|integer|exists:playlists,id,deleted_at,NULL',
            'author'        => 'nullable|integer|max:3'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $select = ['id', 'title', 'enabled', 'created_at'];
        $with   = [
            'image', 
            'playlists' => function($query) use ($request) {
                if(isset($request->playlist_id)) {
                    $query->where('playlists.id', '=', $request->playlist_id);
                }
            }
        ];
            
        if(!$request->page || $request->page == 1) {
            $highlights = Video::select($select)
                ->with($with)
                ->whereHas('playlists', function($query) use ($request) {
                    $query->where('playlists.id', '=', $request->playlist_id);
                    $query->where('playlist_video.highlight', '=', 1);
                })
                ->whereHas('author', function($query) use ($request) {
                    if($request->has('author') && $request->author == 3) {
                        $query->whereId($request->user()->id);
                    }
                });
            if($request->has('author') && $request->author == 1) {
                $highlights->whereOfficial(1);
            } else if($request->has('author') && $request->author == 2) {
                $highlights->whereOfficial(0);
            }
            $highlights = $highlights->get();
            if($highlights) {
                $highlightIDs = array_column($highlights->toArray(), 'id');
            }
        }
        $videos = Video::select($select)
            ->with($with)
            ->whereHas('author', function($query) use ($request) {
                if($request->has('author') && $request->author == 3) {
                    $query->whereId($request->user()->id);
                }
            });
        if($request->has('author') && $request->author == 1) {
            $videos->whereOfficial(1);
        } else if($request->has('author') && $request->author == 2) {
            $videos->whereOfficial(0);
        }
        if(isset($highlightIDs) && count($highlightIDs)) {
            $videos = $videos->whereNotIn('id', $highlightIDs);
        }
        $videos = $videos->orderBy('id', 'DESC')->paginate(50);
        if($videos && isset($highlights) && count($highlights)) {
            foreach ($highlights as $key => $value) {
                $videos->prepend($value);
            }
            $videos = $videos->toArray();
            $videos['total'] += count($highlights); 
        }
        return response()->json(compact('videos'));
    }

    /**
     * Update the specified Playlist Video in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function videoUpdate(Request $request)
    {
        $rules = [
            'playlist_id'           => 'required|max:100|exists:playlists,id,deleted_at,NULL',
            'videos'                => 'array',
            'videos.*.id'           => 'required|integer|exists:videos,id,deleted_at,NULL',
            'videos.*.highlight'    => 'required|boolean',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $playlist = Playlist::find($request->playlist_id);
        $sync = [];
        $highlights = 0;
        if($request->has('videos') && count($request->videos)) {
            foreach ($request->videos as $video) {
                $sync[$video['id']] = [
                    'highlight' => $video['highlight']
                ];
                if(filter_var($video['highlight'], FILTER_VALIDATE_BOOLEAN)) {
                    $highlights++;
                }
            }
        }
        if($highlights > 1) {
            return response()->json([
                'errors' => ['message' => __('messages.playlist_highlight_exceeded')]
            ], 400);
        }
        if($playlist->videos()->sync($sync)) {
            return response()->json([
                'message'   => __('messages.playlist_videos_updated')
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.playlist_videos_uncreated')]
        ], 400);
    }
    
    public function coverTest ()
    {
        return $this->playlistCover();
    }
}
