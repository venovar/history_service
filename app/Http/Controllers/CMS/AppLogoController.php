<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use App\AppLogo;

class AppLogoController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin'])
            ->except('show');
    }
    
    public function show() 
    {   
        $last = AppLogo::orderBy('id', 'DESC')->first();
        if($last && Storage::exists($last->disableUrl()->logo)) {
            $file = Storage::get($last->logo);
            $mimeType = Storage::mimeType($last->logo);
             return (new Response($file, 200))->header('Content-Type', $mimeType);
        }
        $mimeType = 'image/jpeg';
        $path = public_path('image_avatar.jpg');
        $type = pathinfo($path, PATHINFO_EXTENSION);
        $data = file_get_contents($path);
        return (new Response($data, 200))->header('Content-Type', $mimeType);
    }
    /**
     * Update the specified Video in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'file'      => 'required_without:base64|file|image|max:30000|dimensions:max_width=1000',
            'base64'    => 'required_without:file|string',
            'formats'   => 'array',
            'formats.*' => 'in:jpg,jpeg,png,gif',
        ];
        if($request->has('formats') && !empty($request->formats)) {
            $rules['file'] .= '|mimes:' . implode(',', $request->formats);
        } else {
            $rules['file'] .= '|mimes:jpeg,png,gif';
        }
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        /// From file upload.
        if($request->hasFile('file')) {
            $fileExtension  = strtolower($request->file('file')->getClientOriginalExtension());
            $fileName       = $request->file('file')->getClientOriginalName();
            $storageFile    = Storage::put('public', $request->file('file'), 'public');
        } 
        /// From base64 image.
        else {
            preg_match('/data:image\/(.*?);base64/', $request->base64, $match);
            $fileExtension  = strtolower($match[1]);
            $fileName       = sprintf('%s.%s', md5(bcrypt(strtotime('now'))), $fileExtension);
            $storageFile    = sprintf('public/%s', $fileName);
            Storage::put(
                $storageFile,
                base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->base64)), 
                'public'
            );
        }
        $appLogo = new AppLogo();
        $appLogo->author_id   = $request->user()->id;
        $appLogo->logo        = $storageFile;
        $last = AppLogo::orderBy('id', 'DESC')->first();
        if($last) {
            $last->delete();
        }
        if($appLogo->save()) {
            return response()->json([
                'message'       => __('messages.image_uploaded'),
                'app_config'    => $appLogo
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
    
    /**
     * By cron.
     */
    public function forceDelete ()
    {
        // Delete definitely old images.
        $images = AppLogo::withTrashed()
            ->whereDate('deleted_at', '<=', Carbon::yesterday())
            ->get();
        foreach ($images as $key => $image) {
            $image = $image->disableUrl();
            if(Storage::exists($image->logo)) {
                Storage::delete($image->logo);
            }
            $image->forceDelete();
        }
    }
}