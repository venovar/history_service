<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Serie;
use App\Image;
use App\ImageOfNote;

class SerieController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin']);
    }
    /**
     * Display a listing of the Serie.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'init'      => 'nullable|date',
            'end'       => 'nullable|date',
            'highlight' => 'nullable|boolean',
            'enabled'   => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $series = Serie::with(['image']);
        if($request->has('init') && !empty($request->init)) {
            $series->whereDate('conveyed_at', '>=', $request->init);
        } 
        if($request->has('end') && !empty($request->end)) {
            $series->whereDate('conveyed_at', '<=', $request->end);
        }
        if($request->has('enabled') && $request->enabled != null) {
            $series->whereEnabled($request->enabled);
        }
        if($request->has('highlight') && $request->highlight != null) {
            $series->whereHighlight($request->highlight);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $series->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $series->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $series->orderBy('score01', 'DESC');
        }
        $series = $series
            ->orderBy('highlight', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(30);
        return response()->json(compact('series'));
    }
    
    /**
     * Store a newly created Serie in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'         => 'required|max:100',
            'note'          => 'required|string|max:5000',
            'conveyed_at'   => 'required|is_date_time',
            'enabled'       => 'boolean',
            'highlight'     => 'boolean',
            'image.id'      => 'required|integer|exists:images,id,deleted_at,NULL',
            'image_of_notes'        => 'array',
            'image_of_notes.*.id'   => 'required|integer|exists:image_of_notes,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $serie = new Serie();
        $serie->fill($request->all());
        
        $serie->author_id = $request->user()->id;
        
        if($serie->save()) {
            
            if(filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN)) {
                $series = Serie::whereHas('author', function($query) {
                        $query->whereAdmin(1);
                    })
                    ->where('id', '!=', $serie->id)
                    ->update(['highlight' => 0]);
            }
            
            if($request->has('image') && !empty($request->image['id'])) {
                
                $image = Image::find($request->image['id']);
                if($image) {
                    $serie->image()->save($image);
                }
            }
            
            if($request->has('image_of_notes')) {
                foreach ($request->image_of_notes as $imageOfNote) {
                    $serie->imageOfNotes()->save(ImageOfNote::find($imageOfNote['id']));
                }
            }
            
            $serie->author;
            $serie->image;
            $serie->imageOfNotes;
            
            return response()->json([
                'message'   => __('messages.serie_created'),
                'serie' => $serie,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Serie.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:series,id,deleted_at,NULL',
            'markdown'  => 'nullable|boolean'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $serie = Serie::with([
                'author', 
                'image', 
                'imageOfNotes'
            ])->find($request->id);
        
        $serie->setMarkdown($request->markdown);
        
        return response()->json(compact('serie'));
    }

    /**
     * Update the specified Serie in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {
        $rules = [
            'id'            => 'required|integer|exists:series,id,deleted_at,NULL',
            'title'         => 'required|max:100',
            'note'          => 'required|string|max:5000',
            'conveyed_at'   => 'required|is_date_time',
            'enabled'       => 'boolean',
            'highlight'     => 'boolean',
            'image.id'      => 'required|integer|exists:images,id,deleted_at,NULL',
            'image_of_notes'        => 'array',
            'image_of_notes.*.id'   => 'required|integer|exists:image_of_notes,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $serie = Serie::find($request->id);
        $serie->fill($request->all());
        if($serie->save()) {
            
            if(filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN)) {
                $series = Serie::whereHas('author', function($query) {
                        $query->whereAdmin(1);
                    })
                    ->where('id', '!=', $serie->id)
                    ->update(['highlight' => 0]);
            }
            
            if($request->has('image') && !empty($request->image['id'])) {
                
                if($serie->image) {
                    $serie->image->imageable_id = null;
                    $serie->image->imageable_type = null;
                    $serie->image->save();
                }
                
                $serie->image()->save(
                    Image::find($request->image['id'])
                );
            }
            
            if($request->has('image_of_notes')) {
                foreach ($request->image_of_notes as $imageOfNote) {
                    $serie->imageOfNotes()->save(ImageOfNote::find($imageOfNote['id']));
                }
            }
            
            $serie->author;
            $serie->image;
            $serie->imageOfNotes;
            
            return response()->json([
                'message'   => __('messages.serie_updated'),
                'serie' => $serie,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Remove the specified Serie from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:series,id,deleted_at,NULL'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $serie = Serie::find($request->id);
        
        if($serie->delete()) {
            return response()->json([
                'message'   => __('messages.serie_deleted')
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
