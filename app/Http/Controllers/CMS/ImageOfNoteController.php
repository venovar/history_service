<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Facades\Storage;
use App\Http\Traits\ImageJobs;
use Carbon\Carbon;
use App\ImageOfNote;

class ImageOfNoteController extends Controller
{
    use ImageJobs;
    
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin']);
    }
    
    /**
     * Store a newly created Images in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request)
    {
        return $this->imageStore($request, new ImageOfNote(), 'image_of_note');
    }
    
    /**
     * Remove the specified Image from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:image_of_notes,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $image_of_note = ImageOfNote::find($request->id);
        
        if($image_of_note->delete()) {
            return response()->json([
                'message'   => __('messages.image_deleted')
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
    
    /**
     * By cron.
     */
    public function forceDelete ()
    {
        // Delete definitely old images.
        $images = ImageOfNote::withTrashed()
            ->whereDate('deleted_at', '<=', Carbon::yesterday())
            ->get();
        foreach ($images as $key => $image) {
            $image = $image->disableUrl();
            $paths = [];
            if(Storage::exists($image->file)) {
                $paths[] = $image->file;
            }
            if(Storage::exists($image->thumb)) {
                $paths[] = $image->thumb;
            }
            if(Storage::exists($image->static)) {
                $paths[] = $image->static;
            }
            if(count($paths)) {
                Storage::delete($paths);
            }
            $image->forceDelete();
        }
        // Delete empty polimorphs.
        $images = ImageOfNote::withTrashed()
            ->where('imageable_id', '=', null)
            ->whereDate('created_at', '<=', Carbon::yesterday())
            ->get();
        foreach ($images as $key => $image) {
            $image->delete();
        }
    }
}
