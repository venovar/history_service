<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Edujugon\PushNotification\PushNotification as EdPushNotification;
use Carbon\Carbon;
use App\Device;
use App\PushNotification;
use App\Curiosity;

class PushNotificationController extends Controller
{
    /**
     * By cron.
     */
    public function update()
    {
        $pushNotifications = $this->getPushNotificationIDs();
        if(count($pushNotifications)) {
            // Get content to send.
            $pushNotifications = PushNotification::orderBy('id', 'ASC')
                ->with(['pushable', 'device'])
                ->whereIn('id', $pushNotifications)
                ->take(100)
                ->get();
            // Sets the status of the message being sent. Keep toArray() because the take().
            PushNotification::whereIn('id', array_column($pushNotifications->toArray(), 'id'))->update(['sent' => 1]);
            $devicesPushs = [];
            // Order by devices.
            foreach ($pushNotifications as $pushNotification) {
                $element = [
                    'content' => $pushNotification->pushable
                ];
                if(!isset($devicesPushs[$pushNotification->pushable_type . $pushNotification->pushable_id]))
                    $devicePush = [];
                else
                    $devicePush = $devicesPushs[$pushNotification->pushable_type . $pushNotification->pushable_id];
                
                $devicePush['content'] = $pushNotification->pushable->toArray();
                $devicePush['type'] = class_basename($pushNotification->pushable);
                $devicePush['title'] = $this->getElementType($pushNotification->pushable);
                $devicePush['body'] = $devicePush['content']['title'];
                if(class_basename($pushNotification->pushable) == 'Curiosity') {
                    $devicePush['body'] .= chr(10) . 'Veja mais...';
                } else if(class_basename($pushNotification->pushable) == 'Institutional') {
                    $devicePush['title'] = $devicePush['content']['title'];
                    $contentSummary = $devicePush['content']['summary'];
                    $devicePush['body'] = (strlen($contentSummary) > 100) ? substr($contentSummary, 0, 97).'...' : $contentSummary;
                }
                if(!empty($pushNotification->device->token)) {
                    if(!isset($devicePush[$pushNotification->device->platform . '_devices']))
                        $devicePush[$pushNotification->device->platform . '_devices'] = [];
                    if(!in_array($pushNotification->device->token, $devicePush[$pushNotification->device->platform . '_devices']))
                        $devicePush[$pushNotification->device->platform . '_devices'][] = $pushNotification->device->token;
                }
                $devicesPushs[$pushNotification->pushable_type . $pushNotification->pushable_id] = $devicePush;
            }
            // Send notification.
            foreach ($devicesPushs as $devicesPush) {
                if(isset($devicesPush['android_devices']) && count($devicesPush['android_devices'])) {
                    $this->sendAndroidNotification($devicesPush);
                }
                if(isset($devicesPush['ios_devices']) && count($devicesPush['ios_devices'])) {
                    $this->sendIOSNotification($devicesPush);
                }
            }
        }
    }
    
    private function sendIOSNotification($value) 
    {
        $push = new EdPushNotification('apn');
        $push->setConfig(config('app.push_notification.ios'));
        $push->setMessage([
            'aps' => [
                'alert' => [
                    'title' => $value['title'],
                    'body' => $value['body'],
                ],
                'sound' => 'default'
            ],
            'extraPayLoad' => [
                    'id' => $value['content']['id'] ,
                    'target' => $value['type'],
                ]
            ]);
        $push->setDevicesToken($value['ios_devices']);
        $push->send()/*->getFeedback()*/;
    }
    
    private function sendAndroidNotification($value) 
    {
        $push = new EdPushNotification();
        $push->setConfig(config('app.push_notification.android02'));
        $push->setMessage([
            'notification' => [
                    'title' => $value['title'],
                    'body' => $value['body'],
                ],
            'data' => [
                    'id' => $value['content']['id'] ,
                    'target' => $value['type'],
                ]
            ]);
        $push->setDevicesToken($value['android_devices']);
        $push->send()/*->getFeedback()*/;
    }
    
    private function getElementType ($value) 
    {
        switch (class_basename($value)) {
            case 'Curiosity':
                return 'Hoje na História';
                break;
            case 'Playlist':
                return 'Playlist';
                break;
            case 'Article':
                return 'Artigo';
                break;
            case 'Institutional':
                return 'Notificação Institutional';
                break;
            default:
                return null;
        }
    }
    
    /// Todos dias as 9 abastece o stack de push com curiosidades do dia.
    public function curiositiesForToday ()
    {
        /// Lista todos do dia.
        $curiosity = Curiosity::orderBy('id', 'ASC')
            ->whereEnabled(1)
            ->whereRaw($this->dateFilter('conveyed_at', '=', date('m-d')))
            ->first();
        /// Grava na fila para ser enviado.
        if($curiosity) {
            $this->pushNotificationStore($curiosity);
        }
    }
    /// Limpar todas notificações do tipo Curiosidades, pq no ano seguinte será executado novamente.
    public function curiositiesAfterMonth ()
    {
        PushNotification::wherePushableType('App\\Curiosity')
            ->whereDate('created_at', '<=', Carbon::now()->subMonth())
            ->update(['deleted_at' => Carbon::now()]);
    }
    
    private function getPushNotificationIDs () {
        $pushNotifications = [];
        /*$pushNotifications = array_merge(PushNotification::select('id')
                ->whereHas('curiosities', function($query) {
                    $query->whereEnabled(1);
                    $query->whereRaw($this->dateFilter('conveyed_at', '=', date('m-d')));
                })
                ->whereHas('device', function($query) {
                    $query->whereNotNull('token');
                })
                ->whereSent(0)
                ->get()
                ->toArray(), 
            $pushNotifications);
        $pushNotifications = array_merge(PushNotification::select('id')
                ->whereHas('playlists', function($query) {
                    $query->whereEnabled(1);
                })
                ->whereHas('device', function($query) {
                    $query->whereNotNull('token');
                })
                ->whereSent(0)
                ->get()
                ->toArray(), 
            $pushNotifications);*/
        $pushNotifications = array_merge(PushNotification::select('id')
                ->whereHas('institutionals', function($query) {
                    $query->whereEnabled(1);
                    $query->where('conveyed_at', '<=', date('Y-m-d H:i:s'));
                })
                ->whereHas('device', function($query) {
                    $query->whereNotNull('token');
                })
                ->whereSent(0)
                ->get()
                ->toArray(), 
            $pushNotifications);
        /*$pushNotifications = array_merge(PushNotification::select('id')
                ->whereHas('articles', function($query) {
                    $query->whereEnabled(1);
                    $query->where('conveyed_at', '<=', date('Y-m-d H:i:s'));
                })
                ->whereHas('device', function($query) {
                    $query->whereNotNull('token');
                })
                ->whereSent(0)
                ->get()
                ->toArray(), 
            $pushNotifications);*/
        return $pushNotifications;
    }
    
    public function test (Request $request) {
        $rules = [
            'id'        => 'required|integer',
            'target'    => 'required|in:Curiosity,Playlist,Article,Institutional',
            'body'      => 'required|string',
            'title'     => 'required|string',
            'android_device'    => 'required_without:ios_device',
            'ios_device'        => 'required_without:android_device',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $data = [
            'title' => $request->title,
            'body'  => $request->body,
            'type'  => $request->type,
            'content' => [
                'id' => $request->id,
            ]
        ];
        if($request->has('android_device') && !empty($request->android_device)) {
            $data['android_devices'] = [ $request->android_device ];
            $this->sendAndroidNotification($data);
        }
        if($request->has('ios_device') && !empty($request->ios_device)) {
            $data['ios_devices'] = [ $request->ios_device ];
            $this->sendIOSNotification($data);
        }
        return 'Done!';
    }
}
