<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Institutional;
use App\Image;
use App\ImageOfNote;

class InstitutionalController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin']);
    }
    /**
     * Display a listing of the Institutional.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'init'      => 'nullable|date',
            'end'       => 'nullable|date',
            'highlight' => 'nullable|boolean',
            'enabled'   => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $institutionals = Institutional::with(['image']);
        if($request->has('init') && !empty($request->init)) {
            $institutionals->whereDate('conveyed_at', '>=', $request->init);
        } 
        if($request->has('end') && !empty($request->end)) {
            $institutionals->whereDate('conveyed_at', '<=', $request->end);
        }
        if($request->has('enabled') && $request->enabled != null) {
            $institutionals->whereEnabled($request->enabled);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $institutionals->selectRaw(sprintf('*, MATCH(title, summary, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $institutionals->whereRaw(sprintf('MATCH(title, summary, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $institutionals->orderBy('score01', 'DESC');
        }
        $institutionals = $institutionals
            ->orderBy('id', 'DESC')
            ->paginate(30);
        return response()->json(compact('institutionals'));
    }
    
    /**
     * Store a newly created Institutional in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'         => 'required|max:100',
            'note'          => 'required|string|max:5000',
            'enabled'       => 'boolean',
            'image.id'      => 'required|integer|exists:images,id,deleted_at,NULL',
            'image_of_notes'        => 'array',
            'image_of_notes.*.id'   => 'required|integer|exists:image_of_notes,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $institutional = new Institutional();
        $institutional->fill($request->all());
        $institutional->author_id = $request->user()->id;
        if($institutional->save()) {            
            if($request->has('image') && !empty($request->image['id'])) {
                $image = Image::find($request->image['id']);
                if($image) {
                    $institutional->image()->save($image);
                }
            }
            if($request->has('image_of_notes')) {
                foreach ($request->image_of_notes as $imageOfNote) {
                    $institutional->imageOfNotes()->save(ImageOfNote::find($imageOfNote['id']));
                }
            }
            $institutional->author;
            $institutional->image;
            $institutional->imageOfNotes;
            if($institutional->send_notification)
                $this->pushNotificationStore($institutional);
            return response()->json([
                'message'   => __('messages.institutional_created'),
                'institutional' => $institutional,
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Institutional.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:institutionals,id,deleted_at,NULL',
            'markdown'  => 'nullable|boolean'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $institutional = Institutional::with([
                'author', 
                'image', 
                'imageOfNotes'
            ])->find($request->id);
        $institutional->setMarkdown($request->markdown);
        return response()->json(compact('institutional'));
    }

    /**
     * Update the specified Institutional in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {
        $rules = [
            'id'            => 'required|integer|exists:institutionals,id,deleted_at,NULL',
            'title'         => 'required|max:100',
            'note'          => 'required|string|max:5000',
            'enabled'       => 'boolean',
            'image.id'      => 'required|integer|exists:images,id,deleted_at,NULL',
            'image_of_notes'        => 'array',
            'image_of_notes.*.id'   => 'required|integer|exists:image_of_notes,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $institutional = Institutional::find($request->id);
        $institutional->fill($request->all());
        if($institutional->save()) {
            if($request->has('image') && !empty($request->image['id'])) {
                if($institutional->image) {
                    $institutional->image->imageable_id = null;
                    $institutional->image->imageable_type = null;
                    $institutional->image->save();
                }
                $institutional->image()->save(
                    Image::find($request->image['id'])
                );
            }
            if($request->has('image_of_notes')) {
                foreach ($request->image_of_notes as $imageOfNote) {
                    $institutional->imageOfNotes()->save(ImageOfNote::find($imageOfNote['id']));
                }
            }
            $institutional->author;
            $institutional->image;
            $institutional->imageOfNotes;
            if($institutional->send_notification)
                $this->pushNotificationStore($institutional);
            return response()->json([
                'message'   => __('messages.institutional_updated'),
                'institutional' => $institutional,
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Remove the specified Institutional from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:institutionals,id,deleted_at,NULL'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $institutional = Institutional::find($request->id);
        if($institutional->delete()) {
            return response()->json([
                'message'   => __('messages.institutional_deleted')
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
