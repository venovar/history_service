<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Video;
use App\Playlist;

class UserController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin']);
    }
    /**
     * Display a listing of the User.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'init'      => 'nullable|date',
            'end'       => 'nullable|date',
            'enabled'   => 'nullable|boolean',
            'admin'     => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $users = User::orderByRaw('id=? DESC', [$request->user()->id]);
        if($request->has('init') && !empty($request->init)) {
            $users->whereDate('created_at', '>=', $request->init);
        }
        if($request->has('end') && !empty($request->end)) {
            $users->whereDate('created_at', '<=', $request->end);
        }
        if($request->has('enabled') && $request->enabled != null) {
            $users->whereEnabled($request->enabled);
        }
        if($request->has('admin') && $request->admin != null) {
            $users->whereAdmin($request->admin);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $users->selectRaw(sprintf('*, MATCH(name, email) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $users->whereRaw(sprintf('MATCH(name, email) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $users->orderBy('score01', 'DESC');
        }
        $users = $users->paginate(30);
        $users->makeVisible(['enabled', 'admin']);
        return response()->json(compact('users'));
    }

    /**
     * Display the specified User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:users,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $user = User::find($request->id);
        $user->makeVisible(['enabled', 'admin']);
        $user->playlists_quantity = Playlist::whereAuthorId($request->id)->count();
        $user->videos_quantity = Video::whereAuthorId($request->id)->count();
        return response()->json(compact('user'));
    }

    /**
     * Update the specified User in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:users,id,deleted_at,NULL',
            'enabled'   => 'boolean',
            'admin'     => 'boolean',
            'name'      => 'shouldnt_be_present', 
            'email'     => 'shouldnt_be_present', 
            'password'  => 'shouldnt_be_present',
            'confirmed' => 'shouldnt_be_present',
            'avatar'    => 'shouldnt_be_present',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $user = User::find($request->id);
        $user->fill($request->all());
        if($user->save()) {
            $user->makeVisible(['enabled', 'admin']);
            return response()->json([
                'message'   => __('messages.user_updated'),
                'user' => $user,
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Remove the specified User from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:users,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $user = User::find($request->id);
        if($user->delete()) {
            return response()->json([
                'message'   => __('messages.user_deleted')
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
