<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Gif;
use App\Image;

class GifController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin']);
    }
    /**
     * Display a listing of the Gif.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'highlight' => 'nullable|boolean',
            'enabled'   => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $gifs = Gif::with(['image']);
        if($request->has('enabled') && $request->enabled != null) {
            $gifs->whereEnabled($request->enabled);
        }
        if($request->has('highlight') && $request->highlight != null) {
            $gifs->whereHighlight($request->highlight);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $gifs->selectRaw(sprintf('*, MATCH(title) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $gifs->whereRaw(sprintf('MATCH(title) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $gifs->orderBy('score01', 'DESC');
        }
        $gifs = $gifs
            ->orderBy('highlight', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(30);
        return response()->json(compact('gifs'));
    }
    
    /**
     * Store a newly created Gif in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'     => 'required|max:100',
            'enabled'   => 'boolean',
            'highlight' => 'boolean',
            'image.id'  => 'required|integer|exists:images,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $gif = new Gif();
        $gif->fill($request->all());
        
        $gif->author_id = $request->user()->id;
                
        if($gif->save()) {
            
            if($request->has('image') && !empty($request->image['id'])) {
                
                $image = Image::find($request->image['id']);
                if($image) {
                    $gif->image()->save($image);
                }
            }
            $gif->author;
            $gif->image;
            return response()->json([
                'message'   => __('messages.gif_created'),
                'gif' => $gif,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Gif.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:gifs,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $gif = Gif::with(['author', 'image'])->find($request->id);
        
        return response()->json(compact('gif'));
    }

    /**
     * Update the specified Gif in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:gifs,id,deleted_at,NULL',
            'title'     => 'required|max:100',
            'enabled'   => 'boolean',
            'highlight' => 'boolean',
            'image.id'  => 'required|integer|exists:images,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $gif = Gif::find($request->id);
        $gif->fill($request->all());
        
        if($gif->save()) {
            
            if($request->has('image') && !empty($request->image['id'])) {
                
                if($gif->image) {
                    $gif->image->imageable_id = null;
                    $gif->image->imageable_type = null;
                    $gif->image->save();
                }
                
                $image = Image::find($request->image['id']);
                if($image) {
                    $gif->image()->save($image);
                }
            }
            
            $gif->author;
            $gif->image;
            
            return response()->json([
                'message'   => __('messages.gif_updated'),
                'gif' => $gif,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Remove the specified Gif from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:gifs,id,deleted_at,NULL'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $gif = Gif::find($request->id);
        
        if($gif->delete()) {
            return response()->json([
                'message'   => __('messages.gif_deleted')
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
