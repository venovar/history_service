<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Article;
use App\Image;
use App\ImageOfNote;

class ArticleController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin']);
    }
    /**
     * Display a listing of the Article.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'init'      => 'nullable|date',
            'end'       => 'nullable|date',
            'highlight' => 'nullable|boolean',
            'enabled'   => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $articles = Article::with(['image']);
        if($request->has('init') && !empty($request->init)) {
            $articles->whereDate('conveyed_at', '>=', $request->init);
        } 
        if($request->has('end') && !empty($request->end)) {
            $articles->whereDate('conveyed_at', '<=', $request->end);
        }
        if($request->has('enabled') && $request->enabled != null) {
            $articles->whereEnabled($request->enabled);
        }
        if($request->has('highlight') && $request->highlight != null) {
            $articles->whereHighlight($request->highlight);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $articles->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $articles->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $articles->orderBy('score01', 'DESC');
        }
        $articles = $articles
            ->orderBy('highlight', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(30);
        return response()->json(compact('articles'));
    }
    
    /**
     * Store a newly created Article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'         => 'required|max:100',
            'note'          => 'required|string|max:5000',
            'conveyed_at'   => 'required|is_date_time',
            'enabled'       => 'boolean',
            'highlight'     => 'boolean',
            'image.id'      => 'required|integer|exists:images,id,deleted_at,NULL',
            'image_of_notes'        => 'array',
            'image_of_notes.*.id'   => 'required|integer|exists:image_of_notes,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $article = new Article();
        $article->fill($request->all());
        $article->author_id = $request->user()->id;
        if($article->save()) {
            if(filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN)) {
                $articles = Article::whereHas('author', function($query) {
                        $query->whereAdmin(1);
                    })
                    ->where('id', '!=', $article->id)
                    ->update(['highlight' => 0]);
            }
            if($request->has('image') && !empty($request->image['id'])) {
                $image = Image::find($request->image['id']);
                if($image) {
                    $article->image()->save($image);
                }
            }
            if($request->has('image_of_notes')) {
                foreach ($request->image_of_notes as $imageOfNote) {
                    $article->imageOfNotes()->save(ImageOfNote::find($imageOfNote['id']));
                }
            }
            $article->author;
            $article->image;
            $article->imageOfNotes;
            //$this->pushNotificationStore($article);
            return response()->json([
                'message'   => __('messages.article_created'),
                'article' => $article,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Article.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:articles,id,deleted_at,NULL',
            'markdown'  => 'nullable|boolean'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $article = Article::with([
                'author', 
                'image', 
                'imageOfNotes'
            ])->find($request->id);
        
        $article->setMarkdown($request->markdown);
        
        return response()->json(compact('article'));
    }

    /**
     * Update the specified Article in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {
        $rules = [
            'id'            => 'required|integer|exists:articles,id,deleted_at,NULL',
            'title'         => 'required|max:100',
            'note'          => 'required|string|max:5000',
            'conveyed_at'   => 'required|is_date_time',
            'enabled'       => 'boolean',
            'highlight'     => 'boolean',
            'image.id'      => 'required|integer|exists:images,id,deleted_at,NULL',
            'image_of_notes'        => 'array',
            'image_of_notes.*.id'   => 'required|integer|exists:image_of_notes,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $article = Article::find($request->id);
        $article->fill($request->all());
        if($article->save()) {
            if(filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN)) {
                $articles = Article::whereHas('author', function($query) {
                        $query->whereAdmin(1);
                    })
                    ->where('id', '!=', $article->id)
                    ->update(['highlight' => 0]);
            }
            if($request->has('image') && !empty($request->image['id'])) {
                if($article->image) {
                    $article->image->imageable_id = null;
                    $article->image->imageable_type = null;
                    $article->image->save();
                }
                $article->image()->save(
                    Image::find($request->image['id'])
                );
            }
            if($request->has('image_of_notes')) {
                foreach ($request->image_of_notes as $imageOfNote) {
                    $article->imageOfNotes()->save(ImageOfNote::find($imageOfNote['id']));
                }
            }
            $article->author;
            $article->image;
            $article->imageOfNotes;
            //$this->pushNotificationStore($article);
            return response()->json([
                'message'   => __('messages.article_updated'),
                'article' => $article,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Remove the specified Article from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:articles,id,deleted_at,NULL'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $article = Article::find($request->id);
        
        if($article->delete()) {
            return response()->json([
                'message'   => __('messages.article_deleted')
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
