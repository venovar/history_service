<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Curiosity;
use App\Image;
use App\ImageOfNote;

class CuriosityController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin']);
    }
    /**
     * Display a listing of the Curiosity.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'init' => 'nullable|date_filter',
            'end'   => 'nullable|date_filter',
            'highlight' => 'nullable|boolean',
            'enabled'   => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $curiosities = Curiosity::with(['image']);
        if($request->has('init') && !empty($request->init)) {
            $curiosities->whereRaw($this->dateFilter('conveyed_at', '>=', $request->init));
        } 
        if($request->has('end') && !empty($request->end)) {
            $curiosities->whereRaw($this->dateFilter('conveyed_at', '<=', $request->end));
        }
        if($request->has('enabled') && $request->enabled != null) {
            $curiosities->whereEnabled($request->enabled);
        }
        if($request->has('highlight') && $request->highlight != null) {
            $curiosities->whereHighlight($request->highlight);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $curiosities->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $curiosities->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $curiosities->orderBy('score01', 'DESC');
        }
        $curiosities = $curiosities
            ->orderBy('highlight', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(30);
        
        return response()->json(compact('curiosities'));
    }
    
    /**
     * Store a newly created Curiosity in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'         => 'required|max:100',
            'note'          => 'required|string|max:5000',
            'conveyed_at'   => 'required|is_date_time',
            'enabled'       => 'boolean',
            'highlight'     => 'boolean',
            'image.id'      => 'required|integer|exists:images,id,deleted_at,NULL',
            'image_of_notes'        => 'array',
            'image_of_notes.*.id'   => 'required|integer|exists:image_of_notes,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $curiosity = new Curiosity();
        $curiosity->fill($request->all());
        $curiosity->author_id = $request->user()->id;
        if($curiosity->save()) {
            if(filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN)) {
                $curiosities = Curiosity::whereHas('author', function($query) {
                        $query->whereAdmin(1);
                    })
                    ->where('id', '!=', $curiosity->id)
                    ->update(['highlight' => 0]);
            }
            if($request->has('image') && !empty($request->image['id'])) {
                $image = Image::find($request->image['id']);
                if($image) {
                    $curiosity->image()->save($image);
                }
            }
            if($request->has('image_of_notes')) {
                foreach ($request->image_of_notes as $imageOfNote) {
                    $curiosity->imageOfNotes()->save(ImageOfNote::find($imageOfNote['id']));
                }
            }
            $curiosity->author;
            $curiosity->image;
            $curiosity->imageOfNotes;
            /*$this->pushNotificationStore($curiosity);*/
            return response()->json([
                'message'   => __('messages.curiosity_created'),
                'curiosity' => $curiosity,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Curiosity.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:curiosities,id,deleted_at,NULL',
            'markdown'  => 'nullable|boolean'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $curiosity = Curiosity::with([
                'author', 
                'image', 
                'imageOfNotes'
            ])->find($request->id);
        
        $curiosity->setMarkdown($request->markdown);
        
        return response()->json(compact('curiosity'));
    }

    /**
     * Update the specified Curiosity in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {
        $rules = [
            'id'            => 'required|integer|exists:curiosities,id,deleted_at,NULL',
            'title'         => 'required|max:100',
            'note'          => 'required|string|max:5000',
            'conveyed_at'   => 'required|is_date_time',
            'enabled'       => 'boolean',
            'highlight'     => 'boolean',
            'image.id'      => 'required|integer|exists:images,id,deleted_at,NULL',
            'image_of_notes'        => 'array',
            'image_of_notes.*.id'   => 'required|integer|exists:image_of_notes,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $curiosity = Curiosity::find($request->id);
        $curiosity->fill($request->all());
        if($curiosity->save()) {
            if(filter_var($request->highlight, FILTER_VALIDATE_BOOLEAN)) {
                $curiosities = Curiosity::whereHas('author', function($query) {
                        $query->whereAdmin(1);
                    })
                    ->where('id', '!=', $curiosity->id)
                    ->update(['highlight' => 0]);
            }
            if($request->has('image') && !empty($request->image['id'])) {
                if($curiosity->image) {
                    $curiosity->image->imageable_id = null;
                    $curiosity->image->imageable_type = null;
                    $curiosity->image->save();
                }
                $curiosity->image()->save(
                    Image::find($request->image['id'])
                );
            }
            if($request->has('image_of_notes')) {
                foreach ($request->image_of_notes as $imageOfNote) {
                    $curiosity->imageOfNotes()->save(ImageOfNote::find($imageOfNote['id']));
                }
            }
            $curiosity->author;
            $curiosity->image;
            $curiosity->imageOfNotes;
            /*$this->pushNotificationStore($curiosity);*/
            return response()->json([
                'message'   => __('messages.curiosity_updated'),
                'curiosity' => $curiosity,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Remove the specified Curiosity from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:curiosities,id,deleted_at,NULL'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $curiosity = Curiosity::find($request->id);
        
        if($curiosity->delete()) {
            return response()->json([
                'message'   => __('messages.curiosity_deleted')
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
