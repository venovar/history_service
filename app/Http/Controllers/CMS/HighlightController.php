<?php

namespace App\Http\Controllers\CMS;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Highlight;
use App\AttachmentType;
use App\Image;

class HighlightController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware(['auth:api', 'restrict:admin']);
    }
    /**
     * Display a listing of the Highlight.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'enabled'   => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $highlights = Highlight::with(['image']);
        if($request->has('enabled') && $request->enabled != null) {
            $highlights->whereEnabled($request->enabled);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $highlights->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $highlights->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $highlights->orderBy('score01', 'DESC');
        }
        $highlights = $highlights
            ->orderBy('id', 'DESC')
            ->paginate(30);
        return response()->json(compact('highlights'));
    }

    /**
     * Show the form for creating a new Highlight.
     *
     * @return \Illuminate\Http\Response
     */
    public function dataprovider()
    {   
        $attachment_types = AttachmentType::select(['id', 'name'])
            ->where('id', '!=', 1)
            ->get();
        return response()->json(compact('attachment_types'));
    }

    /**
     * Store a newly created Highlight in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'                 => 'required|max:100',
            'note'                  => 'required|max:5000',
            'enabled'               => 'boolean',
            'attachment_type_id'    => 'required|integer|exists:attachment_types,id',
            'attachment_id'         => 'required|integer',
            'image.id'              => 'required|integer|exists:images,id,deleted_at,NULL',
        ];
        if($request->has('attachment_type_id') && $request->has('attachment_id')) {
            $attachment = AttachmentType::find($request->attachment_type_id);
            $attachment = new $attachment->class_reference;
            $rules['attachment_id'] .= sprintf('|exists:%s,id,deleted_at,NULL', $attachment->getTable());
        }
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $highlight = new Highlight();
        $highlight->fill($request->all());
        $highlight->author_id = $request->user()->id;
        
        $order = Highlight::orderBy('id', 'DESC')
            ->where('id', '!=', $highlight->id)
            ->first();
        if($highlight->save()) {
            
            if(isset($attachment)) {
                $attachment = $attachment->find($request->attachment_id);
                $attachment->attach()->save($highlight);
            }
            
            if($request->has('image') && !empty($request->image['id'])) {
                
                $image = Image::find($request->image['id']);
                if($image) {
                    $highlight->image()->save($image);
                }
            }
            
            $highlight->author;
            $highlight->attachable;
            $highlight->image;
            
            return response()->json([
                'message'   => __('messages.highlight_created'),
                'highlight' => $highlight,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Highlight.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:highlights,id,deleted_at,NULL',
            'markdown'  => 'nullable|boolean'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $highlight = Highlight::with(['author', 'image', 'attachable'])->find($request->id);
        if(!empty($highlight->attachable_type)) {
            $attachmentType = AttachmentType::where('class_reference', '=', $highlight->attachable_type)->first();
            $highlight->attachment_type_id = $attachmentType->id;
            $highlight->attachment_id = $highlight->attachable_id;
        }
        $highlight->setMarkdown($request->markdown);
        if($highlight->attachable && method_exists($highlight->attachable, 'setMarkdown')) {
            $highlight->attachable->setMarkdown($request->markdown);
        }
        return response()->json(compact('highlight'));
    }

    /**
     * Update the specified Highlight in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {
        $rules = [
            'id'                    => 'required|integer|exists:highlights,id,deleted_at,NULL',
            'title'                 => 'required|max:100',
            'note'                  => 'required|max:5000',
            'enabled'               => 'boolean',
            'attachment_type_id'    => 'required|integer|exists:attachment_types,id',
            'attachment_id'         => 'required|integer',
            'image.id'              => 'required|integer|exists:images,id,deleted_at,NULL',
        ];
        if($request->has('attachment_type_id') && $request->has('attachment_id')) {
            $attachment = AttachmentType::find($request->attachment_type_id);
            $attachment = new $attachment->class_reference;
            $rules['attachment_id'] .= sprintf('|exists:%s,id,deleted_at,NULL', $attachment->getTable());
        }
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $highlight = Highlight::find($request->id);
        $highlight->fill($request->all());
        if($highlight->save()) {
            if(isset($attachment)) {
                $attachment = $attachment->find($request->attachment_id);
                $attachment->attach()->save($highlight);
            }
            if($request->has('image') && !empty($request->image['id'])) {
                if($highlight->image) {
                    $highlight->image->imageable_id = null;
                    $highlight->image->imageable_type = null;
                    $highlight->image->save();
                }
                $image = Image::find($request->image['id']);
                if($image) {
                    $highlight->image()->save($image);
                }
            }
            
            $highlight->author;
            $highlight->attachable;
            $highlight->image;
            
            return response()->json([
                'message'   => __('messages.highlight_updated'),
                'highlight' => $highlight,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Remove the specified Highlight from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:highlights,id,deleted_at,NULL'
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $highlight = Highlight::find($request->id);
        
        if($highlight->delete()) {
            return response()->json([
                'message'   => __('messages.highlight_deleted')
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
    
    /**
     * Check the specified Attachment.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function attachmentCheck (Request $request)
    {
        $rules = [
            'attachment_type_id'    => 'required|integer|exists:attachment_types,id',
            'attachment_id'         => 'required|integer',
        ];
        if($request->has('attachment_type_id') && $request->has('attachment_id')) {
            $attachment = AttachmentType::find($request->attachment_type_id);
            $attachment = new $attachment->class_reference;
            $rules['attachment_id'] .= sprintf('|exists:%s,id,deleted_at,NULL', $attachment->getTable());
        }
        $validator = Validator::make($request->all(), $rules);
        if($validator->passes() && $attachment) {
            $attachment = $attachment->select(['title'])->find($request->attachment_id);
            return response()->json([
                'status'        => 'success',
                'message'       => __('messages.check_attachment_success'),
                'attachment'    => $attachment,
            ]);
        }
        return response()->json([
            'status'    => 'error',
            'message'   => __('messages.check_attachment_unsuccess'),
        ]);
    }
}