<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Device;
use App\PushNotification;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected function dateFilter ($field, $operator, $date) 
    {
        $date = explode("-", $date);
        if(count($date) == 1) {
            $return = sprintf("DAY(%s) %s %s", $field, $operator, intval($date[0]));
        } else if(count($date) == 2) {
            if(strpos($operator, '=') === false)
                $return = sprintf("MONTH(%s) %s= %s AND DAY(%s) %s %s", $field, $operator, intval($date[0]), $field, $operator, intval($date[1]));
            else
                $return = sprintf("MONTH(%s) %s %s AND DAY(%s) %s %s", $field, $operator, intval($date[0]), $field, $operator, intval($date[1]));
        } else if(count($date) == 3) {
            $return = sprintf("DATE(%s) %s '%s'", $field, $operator, implode('-', $date));
        }
        if(isset($return)) {
            return $return;
        }
        return null;
    }
    
    protected function compareWithRequest ($request, $eloquent)
    {
    	if(!is_array($eloquent))
    		$eloquent = $eloquent->toArray();
    	unset($eloquent['id']);
    	unset($eloquent['created_at']);
    	unset($eloquent['updated_at']);
    	unset($eloquent['deleted_at']);
    	return $eloquent == $request;
    }
    
    public function pushNotificationStore ($target)
    {
        if($target->enabled &&
            (!$target->conveyed_at || 
                class_basename($target) == 'Curiosity' || // Curiosity não deve considerar por data, por conta do mês/dia.
                Carbon::parse($target->conveyed_at) >= Carbon::now())) {
            $devices = Device::orderBy('id', 'ASC');
            switch (class_basename($target)) {
                case 'Curiosity':
                    $devices->where('curiosities', '=', 1);
                    break;
                case 'Playlist':
                    $devices->where('playlists', '=', 1);
                    break;
                case 'Article':
                    $devices->where('articles', '=', 1);
                    break;
                case 'Institutional':
                    $devices->where('institutionals', '=', 1);
                    break;
                default:
                    $devices = null;
                    break;
            }
            if($devices) {
                $devices = $devices->get();
                foreach ($devices as $device) {
                    $pushNotification = PushNotification::/*withTrashed()-> motivo?*/firstOrNew([
                        'device_id'     => $device->id,
                        'pushable_id'   => $target->id,
                        'pushable_type' => sprintf('App\%s', class_basename($target)),
                    ]);
                    //motivo? $pushNotification->deleted_at = null;
                    $pushNotification->save();
                }
            }
        }
    }
    
    public function logClear () 
    {
        $path = storage_path('logs/laravel.log');
        if (file_exists($path)) {
            file_put_contents($path, '');
            return response()->json(['message' => 'Cleared.']);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
    
    public function logShow () 
    {
        $file = storage_path('logs/laravel.log');
        if (\File::isFile($file))
        {
            $file = \File::get($file);
            $response = \Response::make($file, 200);
            $response->header('Content-Type', 'txt/plan');
            return $response;
        }
    }
}
