<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Device;
use App\PushNotification;

class PushNotificationController extends Controller
{
    /**
     * Display a listing of the Push Notification.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {   
        $rules = [
            'device_id' => 'required_without_all:token,uuid|integer|exists:devices,id',
            'token'     => 'required_without_all:uuid,device_id|string|max:500|exists:devices,token',
            'uuid'      => 'required_without_all:token,device_id|string|max:100|exists:devices,uuid',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $device = Device::orderBy('id', 'ASC');
        if($request->has('device_id') && $request->device_id) {
            $device->whereId($request->device_id);
        } else if($request->has('token') && $request->token) {
            $device->whereToken($request->token);
        } else if($request->has('uuid') && $request->uuid) {
            $device->whereUuid($request->uuid);
        } 
        $device = $device->first();
        if($device) {
            $push_notifications = [];
            if($device->curiosities) {
                $push_notifications = array_merge(PushNotification::select('id')
                    ->whereHas('curiosities', function($query) {
                        $query->whereEnabled(1);
                        $query->where('conveyed_at', '<=', date('Y-m-d H:i:s'));
                    })
                    ->whereDeviceId($device->id)
                    ->whereSent(1)
                    ->get()->toArray(), $push_notifications);
            }
            if($device->playlists) {
                $push_notifications = array_merge(PushNotification::select('id')
                    ->whereHas('playlists', function($query) {
                        $query->whereEnabled(1);
                    })
                    ->whereDeviceId($device->id)
                    ->whereSent(1)
                    ->get()->toArray(), $push_notifications);
            }
            if($device->institutionals) {
                $push_notifications = array_merge(PushNotification::select('id')
                    ->whereHas('institutionals', function($query) {
                        $query->whereEnabled(1);
                        $query->where('conveyed_at', '<=', date('Y-m-d H:i:s'));
                    })
                    ->whereDeviceId($device->id)
                    ->whereSent(1)
                    ->get()->toArray(), $push_notifications);
            }
            if($device->articles) {
                $push_notifications = array_merge(PushNotification::select('id')
                    ->whereHas('articles', function($query) {
                        $query->whereEnabled(1);
                        $query->where('conveyed_at', '<=', date('Y-m-d H:i:s'));
                    })
                    ->whereDeviceId($device->id)
                    ->whereSent(1)
                    ->get()
                    ->toArray(), $push_notifications);
            }
            if(count($push_notifications)) {
                $push_notifications = PushNotification::orderBy('id', 'DESC')
                    ->with('pushable')
                    ->whereIn('id', $push_notifications)
                    ->paginate(15);
            }
            return response()->json(compact('push_notifications'));
        } else {
            return response()->json([
                'errors' => ['message' => __('messages.not_find_device')]
            ], 400);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Push Notification.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:push_notifications,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $push_notification = PushNotification::with(['pushable.image', 'pushable.author'])->find($request->id);
        if($push_notification)
            return response()->json(compact('push_notification'));
        return response()->json([
            'errors' => ['message' => __('messages.not_find_push')]
        ], 400);
    }

    /**
     * Remove the specified Push Notification from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'token' => 'required_without:uuid|string|max:500|exists:devices,token',
            'uuid'  => 'required_without:token|string|max:100|exists:devices,uuid',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        if($request->has('uuid') && $request->uuid) 
            $device = Device::firstOrCreate(['uuid' => $request->uuid]);
        else
            $device = Device::firstOrCreate(['token' => $request->token]);
        $rules = [
            'id'    => sprintf('required_without:all|integer|exists:push_notifications,id,device_id,%d,deleted_at,NULL', $device->id),
            'all'   => 'required_without:id|boolean'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        if($request->has('all') && !$request->all) {
            return response()->json([
                'message'   => __('messages.push_undeleted')
            ], 400);
        }
        if($request->has('id') && $request->id) {
            $push_notifications = PushNotification::whereId($request->id)->get();
        } else {
            $push_notifications = PushNotification::whereDeviceId($device->id)->get();
        }
        foreach ($push_notifications as $push_notification) {
            if(!$push_notification->delete()) {
                return response()->json([
                    'errors' => ['message' => __('messages.generic_error')]
                ], 400);
            }
        }
        switch (count($push_notifications)) {
            case 0:
                $message = __('messages.push_undeleted');
                break;
            case 1:
                $message = __('messages.push_deleted');
                break;
            default:
                $message = __('messages.pushs_deleted');
                break;
        }
        return response()->json([
            'message'   => $message
        ]);
    }
}
