<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Serie;
use App\Image;
use App\ImageOfNote;

class SerieController extends Controller
{
    /**
     * Display a listing of the Serie.
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $serie = Serie::with(['image'])
            ->whereEnabled(1)
            ->whereHighlight(1)
            ->orderBy('highlight', 'DESC')
            ->orderBy('id', 'DESC')
            ->first();
        if($serie)
            $serie->makeHidden(['enabled']);
        return response()->json(compact('serie'));
    }
}
