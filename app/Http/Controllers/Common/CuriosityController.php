<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;
use Carbon\Carbon;

use App\Curiosity;
use App\Image;
use App\ImageOfNote;

class CuriosityController extends Controller
{
    /**
     * Display a listing of the Curiosity.
     * Order by dates.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'date'      => 'nullable|date_filter',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $curiosities = ['next' => null, 'data' => null];
        // If there isn't date attribute, it will get the last Curiosity.
        if(!$request->has('date') || empty($request->date)) {
            $last = Curiosity::select(['id', 'conveyed_at'])
                ->whereHighlight(0)
                ->orderByRaw('MONTH(conveyed_at) ASC, DAY(conveyed_at) ASC')
                ->whereRaw($this->dateFilter('conveyed_at', '>=', date('m-d')))
                ->first();
        }
        // Get the speciic range of Curiosity, by date.
        $curiositiesEloquent = Curiosity::with('image')
                ->whereEnabled(1)
                ->whereHighlight(0)
                ->orderBy('conveyed_at', 'ASC');
        if(!$request->has('date') || empty($request->date)) {
            $curiositiesEloquent->whereRaw($this->dateFilter('conveyed_at', '=', $last ? date('m-d', strtotime($last->conveyed_at)) : date('m-d') ));
        } else {
            $curiositiesEloquent->whereRaw($this->dateFilter('conveyed_at', '=', $request->date));
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $curiositiesEloquent->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $curiositiesEloquent->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $curiositiesEloquent->orderBy('score01', 'ASC');
        }
        $curiosities['data'] = $curiositiesEloquent->get();
        if($curiosities['data'])
            $curiosities['data']->makeHidden(['enabled']);
        // Searches the next date to list.
        $next = Curiosity::whereHighlight(0)->orderByRaw('MONTH(conveyed_at) ASC, DAY(conveyed_at) ASC');
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $next->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $next->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $next->orderBy('score01', 'ASC');
        }
        if(count($curiosities['data'])) {
            $next->whereRaw($this->dateFilter( 'conveyed_at', '>', date('m-d', strtotime($curiosities['data'][0]->conveyed_at)) ));
        } else if($request->has('date') && !empty($request->date)) {
            $next->whereRaw($this->dateFilter( 'conveyed_at', '>', $request->date ));
        } else {
            $next->whereRaw($this->dateFilter( 'conveyed_at', '>', date('m-d') ));
        }
        $next = $next->first();
        if(!empty($next))
            $curiosities['next'] = date('m-d', strtotime($next->conveyed_at));
        
        // On the first request (no date setting), will list the Highlights on top of list.
        if(!$request->has('date') || empty($request->date)) {
            $highlights = Curiosity::with(['image']);
            if($request->has('quote') && $request->quote != null) {
                if(strpos($request->quote, '@'))
                    $request->quote = str_replace('@', ' ', $request->quote);
                $highlights->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
                $highlights->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
                $highlights->orderBy('score01', 'ASC');
            }
            $highlights = $highlights
                ->whereHighlight(1)
                ->orderBy('conveyed_at', 'ASC')
                ->get();
            if($highlights)
                $highlights->makeHidden(['enabled']);
            $curiosities['data'] = array_merge($highlights->toArray(), $curiosities['data']->toArray());
        }
        return response()->json(compact('curiosities'));
    }

    /**
     * Display the specified Curiosity.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:curiosities,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $curiosity = Curiosity::with([
                'author', 
                'image'
            ])->find($request->id);
        if($curiosity)
            $curiosity->makeHidden(['enabled']);
        return response()->json(compact('curiosity'));
    }
}