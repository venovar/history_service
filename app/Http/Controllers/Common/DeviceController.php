<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Device;

class DeviceController extends Controller
{
    /**
     * Store a newly created Device Token in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'token'             => 'required_without:uuid|string|max:500',
            'uuid'              => 'required_without:token|string|max:100',
            'platform'          => 'nullable|in:ios,android',
            'curiosities'       => 'boolean',
            'playlists'         => 'boolean',
            'articles'          => 'boolean',
            'institutionals'    => 'boolean',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        if($request->has('uuid') && $request->uuid) 
            $device = Device::firstOrCreate(['uuid' => $request->uuid]);
        else
            $device = Device::firstOrCreate(['token' => $request->token]);
        if($device && Auth::guard('api')->check()) {
            $device->user_id = Auth::guard('api')->user()->id;
        }
        $device->fill($request->all());
        if($device->save()) {
            return response()->json([
                'message'       => __('messages.token_stored'),
                'device'        => $device
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Device Token.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'    => 'required_without_all:token,uuid|integer|exists:devices,id',
            'token' => 'required_without_all:uuid,id|string|max:500',
            'uuid'  => 'required_without_all:token,id|string|max:100',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $device = Device::orderBy('id', 'ASC');
        if($request->has('id') && $request->id) {
            $device->whereId($request->id);
        } else if($request->has('token') && $request->token) {
            $device->whereToken($request->token);
        } else if($request->has('uuid') && $request->uuid) {
            $device->whereUuid($request->uuid);
        } 
        $device = $device->first();
        if($device) {
            return response()->json(compact('device'));
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Update the specified Device Token in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'id'                => 'required|integer|exists:devices,id',
            'token'             => 'required_without:uuid|string|max:500',
            'uuid'              => 'required_without:token|string|max:100',
            'platform'          => 'nullable|in:ios,android',
            'curiosities'       => 'boolean',
            'playlists'         => 'boolean',
            'articles'          => 'boolean',
            'institutionals'    => 'boolean',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $device = Device::find($request->id);
        if($device && Auth::guard('api')->check()) {
            $device->user_id = Auth::guard('api')->user()->id;
        }
        if(isset($device)) {
            $device->fill($request->all());
            if($device->save()) {
                return response()->json([
                    'message'   => __('messages.token_updated'),
                    'device'    => $device
                ]);
            }
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
