<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Institutional;
use App\Image;
use App\ImageOfNote;

class InstitutionalController extends Controller
{
    /**
     * Display a listing of the Institutional.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'init' => 'nullable|date',
            'end'   => 'nullable|date',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $institutionals = Institutional::with(['image'])
            ->orderBy('id', 'DESC');
        if($request->has('init') && !empty($request->init)) {
            $institutionals->whereDate('conveyed_at', '>=', $request->init);
        } 
        if($request->has('end') && !empty($request->end)) {
            $institutionals->whereDate('conveyed_at', '<=', $request->end);
        } 
        $institutionals = $institutionals->paginate(15);
        return response()->json(compact('institutionals'));
    }

    /**
     * Display the specified Institutional.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:institutionals,id,deleted_at,NULL',
            'markdown'  => 'nullable|boolean'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $institutional = Institutional::with([
                'author', 
                'image', 
                'imageOfNotes'
            ])->find($request->id);
        $institutional->setMarkdown($request->markdown);
        return response()->json(compact('institutional'));
    }
}
