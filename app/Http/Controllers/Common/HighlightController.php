<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Highlight;
use App\AttachmentType;
use App\Video;
use App\Image;

class HighlightController extends Controller
{
    /**
     * Display a listing of the Highlight.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $highlights = Highlight::with(['image', 'attachable']);
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $highlights->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $highlights->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $highlights->orderBy('score01', 'DESC');
        }
        $highlights = $highlights
            ->whereEnabled(1)
            ->orderBy('id', 'DESC')
            ->paginate(15);
        if($highlights)
            $highlights->makeHidden(['enabled']);
        return response()->json(compact('highlights'));
    }

    /**
     * Display the specified Highlight.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:highlights,id,deleted_at,NULL'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $highlight = Highlight::with(['author', 'image', 'attachable'])->find($request->id);
        if(!empty($highlight->attachable_type)) {
            $attachmentType = AttachmentType::where('class_reference', '=', $highlight->attachable_type)->first();
            $highlight->attachment_type_id = $attachmentType->id;
            $highlight->attachment_id = $highlight->attachable_id;
        }
        if($highlight)
            $highlight->makeHidden(['enabled']);
        return response()->json(compact('highlight'));
    }
}
