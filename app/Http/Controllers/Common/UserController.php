<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\User;
use App\Video;
use App\Playlist;

class UserController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware(['auth:api']);
    }
    /**
     * Display the specified User.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        return response()->json(['user' => $request->user()]);
    }
}
