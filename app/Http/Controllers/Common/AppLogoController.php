<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use \Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Facades\Storage;
use App\AppLogo;

class AppLogoController extends Controller
{   
    public function show() 
    {   
        $last = AppLogo::orderBy('id', 'DESC')->first();
        if($last && Storage::exists($last->disableUrl()->logo)) {
            $file = Storage::get($last->logo);
            $mimeType = Storage::mimeType($last->logo);
             return (new Response($file, 200))->header('Content-Type', $mimeType);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}