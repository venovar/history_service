<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Http\Traits\ImageJobs;
use App\ImageOfNote;

class ImageOfNoteController extends Controller
{
    use ImageJobs;
    
    public function __construct()
    {
        $this
            ->middleware(['auth:api']);
    }
    
    /**
     * Store a newly created Image Of Note in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store (Request $request)
    {
        return $this->imageStore($request, new ImageOfNote(), 'image_of_note');
    }
    
    /**
     * Remove the specified Image Of Note from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:images,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $image = ImageOfNote::find($request->id);
        
        if($image->delete()) {
            return response()->json([
                'message'   => __('messages.image_deleted')
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
