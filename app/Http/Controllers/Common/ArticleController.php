<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Article;
use App\Image;
use App\ImageOfNote;

class ArticleController extends Controller
{
    /**
     * Display a listing of the Article.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'init'      => 'nullable|date',
            'end'       => 'nullable|date',
            'highlight' => 'nullable|boolean',
            'enabled'   => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $articles = Article::with(['image']);
        if($request->has('init') && !empty($request->init)) {
            $articles->whereDate('conveyed_at', '>=', $request->init);
        } 
        if($request->has('end') && !empty($request->end)) {
            $articles->whereDate('conveyed_at', '<=', $request->end);
        } else {
            $articles->whereDate('conveyed_at', '<=', date('Y-m-d'));
        }
        if($request->has('enabled') && $request->enabled != null) {
            $articles->whereEnabled($request->enabled);
        } else {
            $articles->whereEnabled(1);
        }
        if($request->has('highlight') && $request->highlight != null) {
            $articles->whereHighlight($request->highlight);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $articles->selectRaw(sprintf('*, MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $articles->whereRaw(sprintf('MATCH(title, note) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $articles->orderBy('score01', 'DESC');
        }
        $articles = $articles
            ->orderBy('highlight', 'DESC')
            ->orderBy('id', 'DESC')
            ->paginate(15);
        if($articles)
            $articles->makeHidden(['enabled']);
        return response()->json(compact('articles'));
    }

    /**
     * Display the specified Article.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id'        => 'required|integer|exists:articles,id,deleted_at,NULL',
            'markdown'  => 'nullable|boolean'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $article = Article::with([
                'author', 
                'image', 
                'imageOfNotes'
            ])->find($request->id);
        if($article)
            $article->makeHidden(['enabled']);
        return response()->json(compact('article'));
    }
}
