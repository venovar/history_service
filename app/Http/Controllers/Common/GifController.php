<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use GrahamCampbell\Markdown\Facades\Markdown;

use App\Gif;
use App\Image;

class GifController extends Controller
{
    /**
     * Display a listing of the Gif.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'highlight' => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $gifs = Gif::with(['image']);
        if($request->has('highlight') && $request->highlight != null) {
            $gifs->whereHighlight($request->highlight);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $gifs->selectRaw(sprintf('*, MATCH(title) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $gifs->whereRaw(sprintf('MATCH(title) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $gifs->orderBy('score01', 'DESC');
        }
        $gifs = $gifs
            ->whereEnabled(1)
            ->orderBy('highlight', 'DESC')
            ->orderBy('id', 'DESC');
        if($request->has('page') && $request->page > 1) {
            $gifs = $gifs->paginate(14);
        } else {
            $gifs = $gifs->paginate(15);
        }
        $gifs->makeHidden(['enabled']);
        return response()->json(compact('gifs'));
    }
    
    /**
     * Display the specified Gif.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:gifs,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $gif = Gif::with(['author', 'image'])->find($request->id);
        if($gif)
            $gif->makeHidden(['enabled']);
        return response()->json(compact('gif'));
    }
}
