<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use GrahamCampbell\Markdown\Facades\Markdown;
use App\Http\Traits\ImageJobs;

use App\Video;
use App\Playlist;
use App\Image;

class VideoController extends Controller
{
    use ImageJobs;
    
    public function __construct()
    {
        $this
            ->middleware(['auth:api'])
            ->except(['index', 'show']);
    }
    /**
     * Display a listing of the Video.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $rules = [
            'author'    => 'nullable|integer|max:3|author_auth',
            'enabled'   => 'nullable|boolean',
            'quote'     => 'nullable|string|max:50',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $videos = Video::with(['image', 'author'])
            ->whereHas('author', function($query) use ($request) {
                if($request->has('author') && $request->author == 3) {
                    $query->whereId(Auth::guard('api')->user()->id);
                }
            });
        if($request->has('author') && $request->author == 1) {
            $videos->whereOfficial(1);
        } else if($request->has('author') && $request->author == 2) {
            $videos->whereOfficial(0);
        }
        if($request->has('enabled') && $request->enabled != null) {
            $videos->whereEnabled($request->enabled);
        } else {
            $videos->whereEnabled(1);
        }
        if($request->has('quote') && $request->quote != null) {
            if(strpos($request->quote, '@'))
                $request->quote = str_replace('@', ' ', $request->quote);
            $videos->selectRaw(sprintf('*, MATCH(title) AGAINST ("%s*" IN BOOLEAN MODE) AS score01', $request->quote));
            $videos->whereRaw(sprintf('MATCH(title) AGAINST ("%s*" IN BOOLEAN MODE)', $request->quote));
            $videos->orderBy('score01', 'DESC');
        }
        $videos = $videos
            ->orderBy('id', 'DESC')
            ->paginate(15);
        return response()->json(compact('videos'));
    }
    
    /**
     * Store a newly created Video in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'title'    => 'required|max:100',
            'enabled'  => 'boolean',
            'embed'    => 'required|string|max:300',
            'image.id' => 'required|integer|exists:images,id,deleted_at,NULL',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $video = new Video();
        $video->fill($request->all());
        $video->author_id = $request->user()->id;
        if($video->save()) {
            if($request->has('image') && !empty($request->image['id'])) {
                $image = Image::find($request->image['id']);
                if($image) {
                    $video->image()->save($image);
                }
            }
            $video->author;
            $video->image;
            return response()->json([
                'message'   => __('messages.video_created'),
                'video' => $video,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Display the specified Video.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $rules = [
            'id' => 'required|integer|exists:videos,id,deleted_at,NULL'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $video = Video::with(['author', 'image'])->find($request->id);
        return response()->json(compact('video'));
    }

    /**
     * Update the specified Video in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function update(Request $request)
    {
        $rules = [
            'id'       => sprintf('required|integer|exists:videos,id,author_id,%d,deleted_at,NULL', $request->user()->id),
            'title'    => 'required|max:100',
            'embed'    => 'required|string|max:300',
            'enabled'  => 'boolean',
            'image.id' => 'required|integer|exists:images,id,deleted_at,NULL',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $video = Video::find($request->id);
        $video->fill($request->all());
        
        if($video->save()) {
            
            if($request->has('image') && !empty($request->image['id'])) {
                
                if($video->image) {
                    $video->image->imageable_id = null;
                    $video->image->imageable_type = null;
                    $video->image->save();
                }
                
                $image = Image::find($request->image['id']);
                if($image) {
                    $video->image()->save($image);
                }
            }
            
            $video->author;
            $video->image;
            
            return response()->json([
                'message'   => __('messages.video_updated'),
                'video' => $video,
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }

    /**
     * Remove the specified Video from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $rules = [
            'id' => sprintf('required|integer|exists:videos,id,author_id,%d,deleted_at,NULL', $request->user()->id)
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $video = Video::find($request->id);
        
        if($video->delete()) {
            return response()->json([
                'message'   => __('messages.video_deleted')
            ]);
        }
        
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
    
    public function playlistAdd (Request $request) 
    {
        $rules = [
            'playlist_id'    => sprintf('required|max:100|exists:playlists,id,author_id,%d,deleted_at,NULL', $request->user()->id),
            'video_id'       => sprintf('required|integer|exists:videos,id,deleted_at,NULL'),
            'highlight'      => 'boolean',
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $playlist = Playlist::find($request->playlist_id);
        $playlist->videos()->detach($request->video_id);
        $playlist->videos()->attach([$request->video_id => ['highlight' => !$request->highlight ? 0:1]]);
        if(!$playlist->image || $playlist->image->disableUrl()->static == 'cover') {
            $videos = $playlist->videos;
            $files = [];
            foreach ($videos as $video) {
                $files[] = $video->image->disableUrl()->file;
            }
            if(count($files)) {
                if($playlistCover = $this->playlistCover($files, $request)) {
                    if($playlist->image)
                        $playlist->image->delete();
                    $playlist->image()->save($playlistCover);
                }
            }
        }
        return response()->json([
            'message'   => __('messages.playlist_video_added')
        ]);
    }
    
    public function playlistRemove (Request $request) 
    {
        $rules = [
            'playlist_id'    => sprintf('required|max:100|exists:playlists,id,author_id,%d,deleted_at,NULL', $request->user()->id),
            'video_id'       => sprintf('required|integer|exists:videos,id,deleted_at,NULL'),
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $playlist = Playlist::find($request->playlist_id);
        $playlist->videos()->detach($request->video_id);
        return response()->json([
            'message'   => __('messages.playlist_video_removed')
        ]);
    }
}
