<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\User;

class AccountController extends Controller
{
    public function __construct()
    {
        $this
            ->middleware('auth:api');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {   
        return response()->json(['user' => $request->user()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $rules = [
            'name' => 'required|max:255',
        ];
        
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        
        $user = $request->user();
        
        if($user->name != $request->name) {
            $user->name = $request->name;
            if($user->save()) {
                return response()->json([
                    'message'   => __('messages.account_updated'),
                    'user'      => $user
                ]);
            } else {
                return response()->json([
                    'errors' => [
                        'message' => __('messages.generic_error')
                ]], 400);
            }
        } else {
            return response()->json([
                'message'   => __('messages.account_unchanged'),
            ]);
        }
    }
}
