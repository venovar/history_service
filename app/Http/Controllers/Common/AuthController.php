<?php

namespace App\Http\Controllers\Common;

use App\Http\Controllers\Controller;

use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Socialite;
use App\User;
use Auth;

class AuthController extends Controller
{
    use ThrottlesLogins;
    
    public function __construct()
    {
        $this
            ->middleware(['auth:api'])
            ->only('logout');
    }
    
    /**
     * Authorize a client to access the user's account.
     *
     * @param  ServerRequestInterface  $request
     * @return Response
     */
    public function login (Request $request)
    {
        $rules = [
            'email'     => 'required|email',
            'password'  => 'required'
        ];
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        $data = $request->all();
        if(Auth::attempt($data)) {
            
            $user = Auth::user();
            
            return response()->json([
                'access'    => [
                    'token_type'    => 'Bearer',
                    'access_token'  => $user->api_token,
                ],
                'user'      => $user
            ]);
        }
        return response()->json([
            'errors' => ['message' => 'Usuário e senha inválido'],
        ], 400);
    }
    
    public function facebook (Request $request)
    {
        try {
            if($request->has('token') && $request->token) {
                $socialite = Socialite::driver('facebook')->stateless()->userFromToken($request->token);
            } else if($request->has('code') && $request->code) {
                $socialite = Socialite::driver('facebook')->stateless()->user();
            }
        } 
        catch (\Exception $e) { }
        if((!isset($socialite) || !$socialite) && !$request->access_token) {
            return response()->json([
                'redirect' => Socialite::with('facebook')
                    ->stateless()
                    ->redirect()
                    ->getTargetUrl()
            ]);
            
        } else {
            $user = User::firstOrCreate(['email' => $socialite->email]);
            // Is this a new user?
            if(!$user->api_token) {
                $user->name      = $socialite->name;
                $user->confirmed = 1;
                $user->avatar    = $socialite->getAvatar();
                $user->api_token = str_random(120);
                $user->save();
            }
            
            return response()->json([
                'access'    => [
                    'token_type'    => 'Bearer',
                    'access_token'  => $user->api_token,
                    'redirect'      => $user->admin ? 'cms' : 'common',
                ],
                'user' => $user
            ]);
        }
        return response()->json([
            'errors' => [
                'message' => __('messages.auth_error')
            ]], 400);
    }
    
    public function logout (Request $request)
    {
        $user = $request->user();
        if($user) {
            $user->api_token = str_random(120);
            if($user->save()) {
                return response()->json([
                    'message' => __('messages.logout')
                ]);
            }
        }
        return response()->json([
            'errors' => [
                'message' => __('messages.logout_error')
            ]]);
    }
}