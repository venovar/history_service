<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Validator;
use Intervention\Image\ImageManagerStatic;
use Illuminate\Support\Facades\Storage;
use \Illuminate\Http\Response;
use App\Image;

trait ImageJobs
{
    protected function playlistCover ($images=null, $request)
    {
        if($images && count($images) > 0) {
            $margin = 0;
            $imageWidth = 450;
            $imageHeight = 218;
            $dest = imagecreatefromjpeg(public_path('cover_reference.jpg'));
            $count = 0;
            foreach ($images as $key => $image) {
                $image = imagecreatefromstring(Storage::get($image));
                switch ($key) {
                    case 0:
                        if(count($images) == 1)
                            imagecopymerge($dest, $image, 0, 0, 0, 0, 900, 435, 100);
                        else if(count($images) == 2)
                            imagecopymerge($dest, $image, $margin, $margin, $imageWidth/2, 0, $imageWidth, $imageHeight * 2 + 5, 100);
                        else
                            imagecopymerge($dest, $image, $margin, $margin, $imageWidth/2, $imageHeight/2, $imageWidth, $imageHeight, 100);
                        break;
                    case 1:
                        if(count($images) == 2)
                            imagecopymerge($dest, $image, ($margin * 2) + $imageWidth, $margin, $imageWidth/2, 0, $imageWidth, $imageHeight * 2 + 5, 100);
                        else
                            imagecopymerge($dest, $image, ($margin * 2) + $imageWidth, $margin, $imageWidth/2, $imageHeight/2, $imageWidth, $imageHeight, 100);
                        break;
                    case 2:
                        imagecopymerge($dest, $image, $margin, ($margin * 2) + $imageHeight, $imageWidth/2, $imageHeight/2, $imageWidth, $imageHeight, 100);
                        break;
                    case 3:
                        imagecopymerge($dest, $image, ($margin * 2) + $imageWidth, ($margin * 2) + $imageHeight, $imageWidth/2, $imageHeight/2, $imageWidth, $imageHeight, 100);
                        break;
                }
                imagedestroy($image);
                $count++;
                if($count >= 4)
                    break;
            }
            $base64 = $this->resourceToBase64($dest);
            imagedestroy($dest);
            $fileExtension  = 'jpg';
            $fileName       = sprintf('%s.%s', md5(bcrypt(strtotime('now'))), $fileExtension);
            $storageFile    = sprintf('public/%s', $fileName);
            $imageManagerStatic = ImageManagerStatic::make(base64_decode($base64));
            Storage::put(
                $storageFile,
                (string)$imageManagerStatic->encode($fileExtension, 100), 
                'public'
            );
            $imageManagerStatic->fit(300, 180);
            $fileNameThumb = sprintf('public/thumb/%s.%s', md5(bcrypt(strtotime('now'))), $fileExtension);
            Storage::put(
                $fileNameThumb,
                (string)$imageManagerStatic->encode(), 
                'public'
            );
            $imageEloquent = new Image();
            $imageEloquent->author_id   = $request->user()->id;
            $imageEloquent->name        = $fileName;
            $imageEloquent->file        = $storageFile;
            $imageEloquent->thumb       = $fileNameThumb;
            $imageEloquent->static      = 'cover';
            if($imageEloquent->save()) {
                return $imageEloquent;
            }
        }
        return null;
    }
    
    private function resourceToBase64 ( $resource ) 
    {
        ob_start();
        imagejpeg( $resource );
        $image_data = ob_get_contents();
        ob_end_clean();
        return base64_encode( $image_data );
    }
    
    protected function imageStore ($request, $imageEloquent, $attrReturn)
    {
        $rules = [
            'file'      => 'required_without:base64|file|image|max:30000|dimensions:max_width=1000',
            'base64'    => 'required_without:file|string',
            'formats'   => 'array',
            'formats.*' => 'in:jpg,jpeg,png,gif',
        ];
        if($request->has('formats') && !empty($request->formats)) {
            $rules['file'] .= '|mimes:' . implode(',', $request->formats);
        } else {
            $rules['file'] .= '|mimes:jpeg,png,gif';
        }
        $validator = Validator::make($request->all(), $rules);
        if(!$validator->passes()) {
            return response()->json([
                'errors' => $validator->getMessageBag()->toArray()
            ], 400);
        }
        /// From file upload.
        if($request->hasFile('file')) {
            $fileExtension  = strtolower($request->file('file')->getClientOriginalExtension());
            $fileName       = $request->file('file')->getClientOriginalName();
            $storageFile    = Storage::put('public', $request->file('file'), 'public');
        } 
        /// From base64 image.
        else {
            preg_match('/data:image\/(.*?);base64/', $request->base64, $match);
            $fileExtension  = strtolower($match[1]);
            $fileName       = sprintf('%s.%s', md5(bcrypt(strtotime('now'))), $fileExtension);
            $storageFile    = sprintf('public/%s', $fileName);
            $imageManagerStatic = ImageManagerStatic::make(base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->base64)));
            Storage::put(
                $storageFile,
                (string)$imageManagerStatic->encode($fileExtension, 50), 
                'public'
            );
        }
        $imageManagerStatic = $request->hasFile('file') ? 
            ImageManagerStatic::make($request->file('file')->getPathname()) :
            ImageManagerStatic::make(base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $request->base64)));
        if($fileExtension == 'gif') {
            $imageManagerStatic->fit(600, 315);
            $fileNameStatic     = sprintf('public/static/%s.jpg', md5(bcrypt(strtotime('now'))));
            Storage::put(
                $fileNameStatic,
                (string)$imageManagerStatic->encode(), 
                'public'
            );
        }
        $imageManagerStatic->fit(300, 180);
        $fileNameThumb = sprintf('public/thumb/%s.%s', md5(bcrypt(strtotime('now'))), $fileExtension == 'gif' ? 'jpg' : $fileExtension);
        Storage::put(
            $fileNameThumb,
            (string)$imageManagerStatic->encode(), 
            'public'
        );
        $imageEloquent->author_id   = $request->user()->id;
        $imageEloquent->name        = $fileName;
        $imageEloquent->file        = $storageFile;
        $imageEloquent->thumb       = $fileNameThumb;
        if(isset($fileNameStatic)) {
            $imageEloquent->static   = $fileNameStatic;
        }
        if($imageEloquent->save()) {
            return response()->json([
                'message'       => __('messages.image_uploaded'),
                $attrReturn     => $imageEloquent
            ]);
        }
        return response()->json([
            'errors' => ['message' => __('messages.generic_error')]
        ], 400);
    }
}
