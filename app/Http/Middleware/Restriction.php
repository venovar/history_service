<?php

namespace App\Http\Middleware;

use Closure;

class Restriction
{
    /**
     * Handle an incoming request.
     * It'll restrict depending on the user account type.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {   
        if( $request->user() && 
            $role == 'admin' && 
            $request->user()->admin ) {
            
            return $next($request);
        }
        
        return response()->json([
            'errors' => [
                'message'   => __('messages.restriction'),
            ]
        ], 401);
    }
}
