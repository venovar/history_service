<?php

namespace App\Http\Middleware;

use Closure;

class TargetIntercept
{
    /**
     * Handle an incoming request.
     * Here, we intercept the request the 400 error to change to 404 when the ID is not funded.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $response = $next($request);
        if($response->status() == 400 && 
            isset($response->original['errors']['id']) &&
            (   strrpos(implode('_', array_reverse(explode('/', $request->url()), true)), 'show') == 0 ||
                strrpos(implode('_', array_reverse(explode('/', $request->url()), true)), 'update') == 0 ||
                strrpos(implode('_', array_reverse(explode('/', $request->url()), true)), 'delete') == 0
            )) {
            $response->setStatusCode(404);
        }
        return $response;
    }
}
