<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Gif extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
    	'title',
    	'enabled',
        'highlight',
    ];
    
    protected $hidden = [
        'deleted_at',
    ];
    
    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
    
    public function author ()
    {
    	return $this->belongsTo('App\User', 'author_id');
    }
    
    public function attach()
    {
        return $this->morphOne('App\Highlight', 'attachable');
    }
}
