<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
	protected $primaryKey = 'email';
    public $timestamps = false;
	
    protected $fillable = [
    	'email', 
    	'token'
    ];
    
    public function user () 
    {
    	return $this->belongsTo('App\User', 'email');
    }
}
