<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class Image extends Model
{
	use SoftDeletes;
    
    private $urlDisabled = false;
    
    protected $fillable = [
    	'name',
    	'file',
        'thumb',
        'static',
    ];
    
    protected $hidden = [
        'deleted_at',
    ];
    
    public function imageable()
    {
    	return $this->morphTo();
    }
        
    public function disableUrl ()
    {
        $this->urlDisabled = true;
        return $this;
    }
    
    public function getFileAttribute ()
    {
        $file = $this->attributes['file'];
        if(!$this->urlDisabled) {
            if(Storage::exists($file))
                $file = Storage::url($file);
            if(!strpos($file, 'http')) {
                $file = url($file);
            }
        }
        return $file;
    }
    
    public function getThumbAttribute ()
    {
        $thumb = $this->attributes['thumb'];
        if(!$this->urlDisabled) {
            if(Storage::exists($thumb))
                $thumb = Storage::url($thumb);
            if(!strpos($thumb, 'http')) {
                $thumb = url($thumb);
            }
        }
        return $thumb;
    }
    
    public function getStaticAttribute ()
    {
        $static = $this->attributes['static'];
        if(!$this->urlDisabled) {
            if(Storage::exists($static))
                $static = Storage::url($static);
            if(!strpos($static, 'http')) {
                $static = url($static);
            }
        }
        return $static;
    }
}