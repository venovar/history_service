<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Video extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
    	'title',
    	'enabled',
        'embed',
        'official'
    ];
    
    protected $hidden = [
        'deleted_at',
    ];
    
    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
    
    public function author ()
    {
    	return $this->belongsTo('App\User', 'author_id');
    }
    
    public function playlists ()
    {
        return $this
            ->belongsToMany('App\Playlist')
            ->using('App\Pivots\PlaylistVideo')
            ->withPivot('highlight')
            ->withTimestamps();
    }
    
    public function attach()
    {
        return $this->morphOne('App\Highlight', 'attachable');
    }
    
    public function setMarkdown ($flag=true) 
    {
        //
    }
}