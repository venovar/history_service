<?php

namespace App\Validators;
 
use Illuminate\Validation\Validator;
use Carbon\Carbon;

class CustomValidator extends Validator 
{
    public function validateRg ($attribute, $value, $parameters)
    {
        if (!empty($value) && !count($this->messages->toArray())) {
            $value = str_replace([' ', '.', '-', '_'], '', $value);
            if(strlen(preg_replace("/(.)\\1+/", "$1", $value)) < 8)
                return false;
        }
        return true;
    }
    
    public function validateDateFilter ($attribute, $value, $parameters)
    {
        if (!empty($value) && !count($this->messages->toArray())) {
            $split = explode('-', $value);
            if(!count($split))
                return false;
            $split = array_reverse($split);
            foreach ($split as $key => $splitValue) {
                if(!intval($splitValue)) {
                    return false;
                }
                switch ($key) {
                    // Day
                    case 0:
                        if(intval($splitValue) < 1 || intval($splitValue) > 31)
                            return false;
                        break;
                    // Month
                    case 1:
                        if(intval($splitValue) < 1 || intval($splitValue) > 12)
                            return false;
                        break;
                    // Year
                    case 2:
                        if(strlen($splitValue) != 4 || intval($splitValue) < 0 || intval($splitValue) > intval(Carbon::now()->addYears(100)->format('Y')))
                            return false;
                        break;
                }
            }
        }
        return true;
    }

    public function validateIsDate ($attribute, $value, $parameters)
    {
        if(strrpos($value, '/') !== false) {

            $date = \DateTime::createFromFormat('d/m/Y', $value);

            if($date && $date->format('d/m/Y') != $value)
                return false;
            
        } else {

            $date = \DateTime::createFromFormat('Y-m-d', $value);

            if($date && $date->format('Y-m-d') != $value)
                return false;
        }

        return !(!$date || $date->format('Y') > date('Y') + 100 || $date->format('Y') < date('Y') - 100);
    }

    public function validateIsDateTime ($attribute, $value, $parameters)
    {
        return preg_match("/\\A(?:^((\\d{2}(([02468][048])|([13579][26]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])))))|(\\d{2}(([02468][1235679])|([13579][01345789]))[\\-\\/\\s]?((((0?[13578])|(1[02]))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(3[01])))|(((0?[469])|(11))[\\-\\/\\s]?((0?[1-9])|([1-2][0-9])|(30)))|(0?2[\\-\\/\\s]?((0?[1-9])|(1[0-9])|(2[0-8]))))))(\\s(((0?[0-9])|(1[0-9])|(2[0-3]))\\:([0-5][0-9])((\\s)|(\\:([0-5][0-9])))?))?$)\\z/", $value);
    }

    public function validateCpf ($attribute, $value, $parameters)
    {
        if (!empty($value) && !count($this->messages->toArray())) {
            /* Ex.: 23036735852 */
            preg_match_all('/\d+/', $value, $matches);
            $cpf = implode('', $matches[0]);

            if (strlen($cpf) != 11) {
                return false;
            }
            // Elimina cpfs invalidos conhecidos (1111111111)
            if(strlen(preg_replace("/(.)\\1+/", "$1", $cpf)) <= 1) 
                return false;

            $a = array();
            $b = 0;
            $c = 11;
            for ($i=0; $i<11; $i++){
                $a[$i] = substr($cpf,$i,1);
                if ($i < 9) $b += ($a[$i] * --$c);
            }
            if (($x = $b % 11) < 2) {
                $a[9] = 0;
            } else { 
                $a[9] = 11-$x; 
            }
            $b = 0;
            $c = 11;
            for ($y=0; $y<10; $y++) $b += ($a[$y] * $c--);
                if (($x = $b % 11) < 2) { $a[10] = 0; } else { $a[10] = 11-$x; }

            $retorno = true;
            if (substr($cpf,9, 1) != (string)$a[9] || substr($cpf,10,1) != (string)$a[10])
                $retorno = false;
            return $retorno;
        }
        return true;
    }

    public function validateCnpj ($attribute, $value, $parameters)
    {
        if (!count($this->messages->toArray())) {
            /* Ex.: 11.655.744/000-160 */
            $cnpj = preg_replace('/[^\d]+/', '', $value);
            if ($cnpj == '') return false;
            if (strlen($cnpj) < 14)
                return false;
            // Elimina CNPJs invalidos conhecidos (1111111111)
            if(strlen(preg_replace("/(.)\\1+/", "$1", $cnpj)) <= 1) 
                return false;

            // Valida DVs
            $tamanho = strlen($cnpj) - 2;
            $numeros = substr($cnpj, 0, $tamanho);
            $digitos = substr($cnpj, $tamanho);
            $soma = 0;
            $pos = $tamanho - 7;
            for ($i = $tamanho; $i >= 1; $i--) {
                $soma += substr($numeros, $tamanho - $i, 1) * $pos--;
                if ($pos < 2)
                    $pos = 9;
            }
            $resultado = $soma % 11 < 2 ? 0 : 11 - $soma % 11;
            if ($resultado != substr($digitos, 0, 1))
                return false;

            $tamanho = $tamanho + 1;
            $numeros = substr($cnpj, 0, $tamanho);
            $soma = 0;
            $pos = $tamanho - 7;
            for ($i = $tamanho; $i >= 1; $i--) {
                $soma += substr($numeros, $tamanho - $i, 1) * $pos--;
                if ($pos < 2)
                    $pos = 9;
            }
            $resultado = $soma % 11 < 2 ? 0 : 11 - $soma % 11;
            if ($resultado != substr($digitos, 1, 1))
                return false;
        }

        return true;
    }

    public function validateCep ($attribute, $value, $parameters)
    {
        if (!count($this->messages->toArray())) {
            preg_match_all('/\d+/', $value, $matches);
            $value = implode('', $matches[0]);
            return is_numeric($value) && strlen($value) == 8;
        }
        return true;
    }

    public function validateCnhType ($attribute, $value, $parameters)
    {
        return ctype_alpha($value) && strlen($value) <= 2 && in_array(strtoupper($value), [
            'A', 
            'B', 
            'C', 
            'D', 
            'E', 
            'AB', 
            'AC', 
            'AD', 
            'AE'
        ]);
    }
    
    public function validateUntilToday ($attribute, $value, $parameters)
    {
        return date('Y-m-d', strtotime($value)) <= date('Y-m-d');
    }
    
    public function validateShouldntBePresent ($attribute, $value, $parameters)
    {        
        if (!count($this->messages->toArray()) && !empty($attribute)) {
            return false;
        }
        return true;
    }
    
    public function validatePhoneLength ($attribute, $value, $parameters)
    {
        $this->requireParameterCount(2, $parameters, 'phone_length');
        if (!count($this->messages->toArray()) && !empty($attribute)) {
            preg_match_all('/\d+/', $value, $matches);
            $value = implode('', $matches[0]);
            if(strlen($value) < $parameters[0] || strlen($value) > $parameters[1]) {
                return false;
            }
        }
        return true;
    }
    
    public function validateStrongPassword ($attribute, $value, $parameters)
    {
        if ( !count($this->messages->toArray()) ) {
            if(
                !filter_var($parameters[0], FILTER_VALIDATE_BOOLEAN) /// force_weak_password
                &&
                (
                    !preg_match_all('/[A-Z]/', $value,$match) || 
                    !preg_match_all('/[a-z]/', $value,$match) ||
                    !preg_match_all('/[0-9]/', $value,$match) ||
                    !preg_match("/\b[+&@#\/%?=~_|!:,.;]/i",$value,$match)
                )
            ) {
                return false;
            }
        }
        return true;
    }
    
    public function validateVerifyToken ($attribute, $value, $parameters)
    {
        return count(\DB::table('users')->whereNull('deleted_at')->whereRaw(sprintf('md5(email) = "%s"', $value))->get());
    }
    
    public function validateAuthorAuth ($attribute, $value, $parameters)
    {
        if(!count($this->messages->toArray()) && $value == 3 && !\Auth::guard('api')->user())
            return false;
        return true;
    }
    
    public function validateMpx ($attribute, $value, $parameters) 
    {
        $regex = "/http(s)?:\/\/player\.theplatform.com/";
        return preg_match($regex, $value);
    }
}