<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password',
        'confirmed',
        'enabled',
        'admin',
        'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
        'confirmed',
        'admin',
        'enabled',
        'api_token',
        'deleted_at',
    ];
    
    public function setEmailAttribute ($value)
    {
        $this->attributes['email'] = strtolower($value);
    }
    
    public function findForPassport($username) {
        return $this
            ->whereEmail($username)
            ->whereConfirmed(1)
            ->whereEnabled(1)
            ->first();
    }
    
    public function devices ()
    {
        return $this->hasMany('App\Device');
    }
}
