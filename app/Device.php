<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $fillable = [
    	'user_id',
    	'token',
        'uuid',
        'platform',
    	'curiosities',
    	'playlists',
    	'articles',
    	'institutionals',
    ];
    
    protected $hidden = [
    	'user_id',
        'updated_at',
        'created_at',
    ];
    
    public function user ()
    {
    	return $this->belongsTo('App\User');
    }
    
    public function pushNotifications ()
    {
        return $this->hasMany('App\PushNotification');
    }
}
