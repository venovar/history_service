<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class AppLogo extends Model
{
    use SoftDeletes;
    
    private $urlDisabled = false;
    
    protected $fillable = [
        'logo',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    public function disableUrl ()
    {
        $this->urlDisabled = true;
        return $this;
    }
    
    public function getLogoAttribute ()
    {
        $logo = $this->attributes['logo'];
        if(!$this->urlDisabled) {
            if(Storage::exists($logo))
                $logo = Storage::url($logo);
            if(!strpos($logo, 'http')) {
                $logo = url($logo);
            }
        }
        return $logo;
    }
}
