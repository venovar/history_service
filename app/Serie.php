<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use GrahamCampbell\Markdown\Facades\Markdown;

class Serie extends Model
{
    use SoftDeletes;
    
    private $markdown = true;
    
    protected $fillable = [
    	'title',
    	'note',
    	'conveyed_at',
    	'enabled',
    	'highlight',
    ];
    
    protected $hidden = [
        'deleted_at',
    ];
    
    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
    
    public function imageOfNotes()
    {
        return $this->morphMany('App\ImageOfNote', 'imageable');
    }
    
    public function author ()
    {
    	return $this->belongsTo('App\User', 'author_id');
    }
    
    public function getNoteAttribute ()
    {
        if($this->markdown) {
            return Markdown::convertToHtml($this->attributes['note']);
        } else {
            return $this->attributes['note'];
        }
    }
    
    public function setMarkdown ($flag=true)
    {
        if($flag != null)
            $this->markdown = filter_var($flag, FILTER_VALIDATE_INT);
    }
    
    public function attach()
    {
        return $this->morphOne('App\Highlight', 'attachable');
    }
}
