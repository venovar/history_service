<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*
        * CLEAR IMAGES SOURCE.
        */
        $schedule
            ->call('App\Http\Controllers\CMS\ImageController@forceDelete')
            ->at('00:00:00');
        $schedule
            ->call('App\Http\Controllers\CMS\ImageOfNoteController@forceDelete')
            ->at('00:30:00');
        $schedule
            ->call('App\Http\Controllers\CMS\AppLogoController@forceDelete')
            ->at('01:00:00');
        $schedule
            ->call('App\Http\Controllers\CMS\PushNotificationController@curiositiesAfterMonth')
            ->at('01:30:00');
        /*$schedule
            ->call('App\Http\Controllers\CMS\PushNotificationController@curiositiesForToday')
            ->at('09:00:00');*/
        $schedule
            ->call('App\Http\Controllers\CMS\PushNotificationController@update')
            ->everyMinute();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
