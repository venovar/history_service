<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PushNotification extends Model
{
    use SoftDeletes;
    
	protected $fillable = [
		'device_id',
		'sent',
        'pushable_id',
        'pushable_type'
	];
	
    public function pushable()
    {
        return $this->morphTo();
    }
    
    public function device()
    {
    	return $this->belongsTo('App\Device');
    }
    
    public function curiosities()
    {
        return $this->belongsTo('App\Curiosity', 'pushable_id')->wherePushableType('App\\Curiosity');
    }
    
    public function playlists()
    {
        return $this->belongsTo('App\Playlist', 'pushable_id')->wherePushableType('App\\Playlist');
    }
    
    public function articles()
    {
        return $this->belongsTo('App\Article', 'pushable_id')->wherePushableType('App\\Article');
    }
    
    public function institutionals()
    {
        return $this->belongsTo('App\Institutional', 'pushable_id')->wherePushableType('App\\Institutional');
    }
}
