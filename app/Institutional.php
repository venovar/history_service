<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use GrahamCampbell\Markdown\Facades\Markdown;

class Institutional extends Model
{
    use SoftDeletes;
    
    private $markdown = true;
    
    protected $fillable = [
        'title',
        'summary',
        'conveyed_at',
        'note',
        'enabled',
        'send_notification',
    ];
    
    protected $hidden = [
        'deleted_at',
    ];
    
    public function image()
    {
        return $this->morphOne('App\Image', 'imageable');
    }
    public function imageOfNotes()
    {
        return $this->morphMany('App\ImageOfNote', 'imageable');
    }
    public function author ()
    {
        return $this->belongsTo('App\User', 'author_id');
    }
    public function getNoteAttribute ()
    {
        if($this->markdown) {
            return Markdown::convertToHtml($this->attributes['note']);
        } else {
            return $this->attributes['note'];
        }
    }
    public function setMarkdown ($flag=true)
    {
        if($flag != null)
            $this->markdown = filter_var($flag, FILTER_VALIDATE_INT);
    }
    public function setDispatchAtAttribute ($value) 
    {
        $this->attributes['dispatch_at'] = gmdate('Y-m-d', strtotime($value));
    }
}