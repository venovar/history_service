<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => ':attribute deve ser aceito.',
    'active_url'           => ':attribute não é uma URL válida.',
    'after'                => ':attribute deve ser uma data depois de :date.',
    'after_or_equal'       => ':attribute deve ser uma data depois ou igual a :date.',
    'alpha'                => ':attribute deve conter somente letras.',
    'alpha_dash'           => ':attribute deve conter letras, números e traços.',
    'alpha_num'            => ':attribute deve conter somente letras e números.',
    'array'                => ':attribute deve ser um array.',
    'before'               => ':attribute deve ser uma data antes de :date.',
    'before_or_equal'      => ':attribute deve ser uma data antes ou igual a :date.',
    'between'              => [
        'numeric' => ':attribute deve estar entre :min e :max.',
        'file'    => ':attribute deve estar entre :min e :max kilobytes.',
        'string'  => ':attribute deve estar entre :min e :max caracteres.',
        'array'   => ':attribute deve ter entre :min e :max itens.',
    ],
    'boolean'              => ':attribute deve ser verdadeiro ou falso.',
    'confirmed'            => 'A confirmação de :attribute não confere.',
    'date'                 => ':attribute não é uma data válida.',
    'date_format'          => ':attribute não confere com o formato :format.',
    'is_date'              => ':attribute não é uma data válida.', 
    'is_date_time'         => 'O campo :attribute não é uma data ou horário válido.', 
    'date_filter'          => 'O campo Data não é uma data válida.', 
    'different'            => ':attribute e :other devem ser diferentes.',
    'digits'               => ':attribute deve ter :digits dígitos.',
    'digits_between'       => ':attribute deve ter entre :min e :max dígitos.',
    'dimensions'           => ':attribute não tem dimensões válidas. Deve ter no máximo :max_width pixels de largura.',
    'distinct'             => ':attribute campo contém um valor duplicado.',
    'email'                => 'E-mail deve ser um endereço de e-mail válido.',
    'exists'               => ':attribute selecionado(a) é inválido.',
    'file'                 => ':attribute precisa ser um arquivo.',
    'filled'               => ':attribute é um campo obrigatório.',
    'image'                => ':attribute deve ser uma imagem.',
    'in'                   => ':attribute é inválido.',
    'in_array'             => ':attribute campo não existe em :other.',
    'integer'              => ':attribute deve ser um inteiro.',
    'ip'                   => ':attribute deve ser um endereço IP válido.',
    'ipv4'                 => ':attribute deve ser um endereço IP4 válido.',
    'ipv6'                 => ':attribute deve ser um endereço IP6 válido.',
    'json'                 => ':attribute deve ser um JSON válido.',
    'max'                  => [
        'numeric' => ':attribute não deve ser maior que :max.',
        'file'    => ':attribute não deve ter mais que :max kilobytes.',
        'string'  => ':attribute não deve ter mais que :max caracteres.',
        'array'   => ':attribute não pode ter mais que :max itens.',
    ],
    'mimes'                => ':attribute deve ser um arquivo do tipo: :values.',
    'mimetypes'            => ':attribute deve ser um arquivo do tipo: :values.',
    'min'                  => [
        'numeric' => ':attribute deve ser no mínimo :min.',
        'file'    => ':attribute deve ter no mínimo :min kilobytes.',
        'string'  => ':attribute deve ter no mínimo :min caracteres.',
        'array'   => ':attribute deve ter no mínimo :min itens.',
    ],
    'not_in'               => 'O :attribute selecionado é inválido.',
    'numeric'              => ':attribute deve ser um número.',
    'present'              => 'O :attribute deve estar presente.',
    'regex'                => 'O formato de :attribute é inválido.',
    'required'             => 'O campo :attribute é obrigatório.',
    'required_if'          => 'O campo :attribute é obrigatório quando :other é :value.',
    'required_unless'      => 'O :attribute é necessário a menos que :other esteja em :values.',
    'required_with'        => 'O campo :attribute é obrigatório quando :values está presente.',
    'required_with_all'    => 'O campo :attribute é obrigatório quando :values estão presentes.',
    'required_without'     => 'O campo :attribute é obrigatório quando :values não está presente.',
    'required_without_all' => 'O campo :attribute é obrigatório quando nenhum destes estão presentes: :values.',
    'same'                 => ':attribute e :other devem ser iguais.',
    'size'                 => [
        'numeric' => ':attribute deve ser :size.',
        'file'    => ':attribute deve ter :size kilobytes.',
        'string'  => ':attribute deve ter :size caracteres.',
        'array'   => ':attribute deve conter :size itens.',
    ],
    'string'               => ':attribute deve ser uma string',
    'timezone'             => ':attribute deve ser uma timezone válida.',
    'unique'               => 'Este :attribute já está em uso.',
    'uploaded'             => ':attribute falhou no upload.',
    'url'                  => 'O formato de :attribute é inválido.',
    'until_today'          => ':attribute não pode ser maior que hoje.',
    'cpf'                  => 'CPF inválido.',
    'rg'                   => 'RG inválido.',
    'shouldnt_be_present'  => 'O :attribute não deve estar presente.',
    'phone_length'         => 'O :attribute é inválido.',
    'strong_password'      => 'Sua senha é vulnerável. Deseja realmente prosseguir?',
    'author_auth'          => 'Você não possui permissão para acessar.',
    'token'                => ':attribute inválido.',
    'mpx'                   => 'Não é uma url de vídeo MPX válido.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'token' => [
            'verify_token'  => 'O acesso está obsoleto.',
            'required'      => 'O acesso é inválido.'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [
        'id'                => 'Código',
        'name'              => 'Nome',
        'email'             => 'E-mail',
        'birthdate'         => 'Nascimento',
        'current_password'      => 'Senha Atual',
        'password'              => 'Senha',
        'password_confirmation' => 'Confirmação da Senha',
        'rg'                => 'RG',
        'cpf'               => 'CPF',
        'type_id'           => 'Tipo',
        'photo'             => 'Foto',
        'amount'            => 'Quantidade',
        'phone'             => 'Telefone',
        'user_id'           => 'Usuário',
        'note'              => 'Observações',
        'title'             => 'Título',
        'token'             => 'Token',
        'address'           => 'Endereço',
        'complement'        => 'Complemento',
        'city'              => 'Cidade',
        'attachment_id'         => 'Anexo',
        'attachment_type_id'    => 'Tipo de Anexo',
        'image'             => 'Imagem',
        'image.id'          => 'Imagem',
        'embed'             => 'Vídeo',
        'playlist_id'       => 'Playlist',
        'file'              => 'Arquivo',
        'base64'            => 'Arquivo',
        'conveyed_at'       => 'Data',
        'image_of_notes'        => 'Imagem',
        'image_of_notes.*.id'   => 'Imagem',
        'admin'             => 'Administrador',
        'enabled'           => 'Campo "Ativo"',
        'uuid'              => 'UUID',
        'device_id'         => 'Device',
        'mpx'               => 'MPX',
        'is_date_time'      => 'Data',
        'is_date'           => 'Data',
        'platform'          => 'Plataforma',
    ],
];