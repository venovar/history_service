<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('devices', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->nullable()->index('fk_device_user_idx');
			$table->string('token', 500)->nullable();
			$table->string('uuid', 100)->nullable()->index('device_uuid_index');
			$table->boolean('curiosities')->nullable()->default(0);
			$table->boolean('playlists')->nullable()->default(0);
			$table->boolean('articles')->nullable()->default(0);
			$table->boolean('institutionals')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('devices');
	}

}
