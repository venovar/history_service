<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSeriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('series', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('author_id')->nullable()->index('fk_tune_in_author_idx');
			$table->string('title', 100);
			$table->dateTime('conveyed_at')->nullable();
			$table->text('note')->nullable();
			$table->boolean('enabled')->default(1);
			$table->boolean('highlight')->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('series');
	}

}
