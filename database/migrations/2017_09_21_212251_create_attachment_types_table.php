<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttachmentTypesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('attachment_types', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 50);
			$table->string('class_reference', 50);
		});
		DB::table('attachment_types')->insert([
	        [
	            'name' 				=> 'Destaques',
	            'class_reference' 	=> 'App\\Highlight'
	        ],
	        [
	            'name' 				=> 'Videos',
	            'class_reference' 	=> 'App\\Video'
	        ],
	        [
	            'name' 				=> 'Playlist',
	            'class_reference' 	=> 'App\\Playlist'
	        ],
	        [
	            'name' 				=> 'Gifs',
	            'class_reference' 	=> 'App\\Gif'
	        ],
	        [
	            'name' 				=> 'Hoje na História',
	            'class_reference' 	=> 'App\\Curiosity'
	        ],
	        [
	            'name' 				=> 'Artigos',
	            'class_reference' 	=> 'App\\Article'
	        ]
	    ]);
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('attachment_types');
	}

}
