<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateImagesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('images', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('author_id')->nullable()->index('fk_img_author_idx');
			$table->string('name', 200);
			$table->string('file', 200);
			$table->string('thumb', 200)->nullable();
			$table->string('static', 200)->nullable();
			$table->integer('imageable_id')->nullable();
			$table->string('imageable_type', 50)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->index(['imageable_id','imageable_type'], 'imageable_index');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('images');
	}

}
