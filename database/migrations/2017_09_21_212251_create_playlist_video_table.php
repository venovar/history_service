<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlaylistVideoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('playlist_video', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('playlist_id')->nullable()->index('fk_pv_playlist_idx');
			$table->integer('video_id')->nullable()->index('fk_pv_video_idx');
			$table->boolean('highlight')->nullable()->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('playlist_video');
	}

}
