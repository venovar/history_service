<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstitutionalsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('institutionals', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('author_id')->nullable()->index('fk_institutional_author_idx');
			$table->string('title', 100);
			$table->text('summary', 65535)->nullable();
			$table->dateTime('conveyed_at')->nullable();
			$table->text('note')->nullable();
			$table->boolean('enabled')->nullable()->default(1);
			$table->boolean('send_notification')->nullable()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('institutionals');
	}

}
