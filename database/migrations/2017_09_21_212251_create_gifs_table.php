<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGifsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gifs', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('author_id')->nullable()->index('fk_gif_author_idx');
			$table->string('title', 100);
			$table->boolean('enabled')->nullable()->default(1);
			$table->boolean('highlight')->nullable()->default(0);
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gifs');
	}

}
