<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToPushNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('push_notifications', function(Blueprint $table)
		{
			$table->foreign('device_id', 'fk_push_device')->references('id')->on('devices')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('push_notifications', function(Blueprint $table)
		{
			$table->dropForeign('fk_push_device');
		});
	}

}
