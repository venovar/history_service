<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHighlightsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('highlights', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('author_id')->nullable()->index('fk_highlight_author_idx');
			$table->string('title', 100);
			$table->text('note')->nullable();
			$table->boolean('enabled')->nullable()->default(1);
			$table->integer('attachable_id')->nullable();
			$table->string('attachable_type', 100)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->index(['attachable_id','attachable_type'], 'highlightable_index');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('highlights');
	}

}
