<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePushNotificationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('push_notifications', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('device_id')->nullable()->index('fk_push_device_idx');
			$table->integer('pushable_id')->nullable();
			$table->string('pushable_type', 50)->nullable();
			$table->boolean('sent')->nullable()->comment('0 = Ainda não entregue.
1 = Entregue.')->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->index(['pushable_id','pushable_type'], 'pushable_index');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('push_notifications');
	}

}
