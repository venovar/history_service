<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('name', 200)->nullable();
			$table->string('email', 100)->unique('email_UNIQUE');
			$table->string('password', 200)->nullable();
			$table->string('api_token', 120)->nullable();
			$table->boolean('confirmed')->nullable()->default(0);
			$table->string('remember_token', 100)->nullable();
			$table->boolean('admin')->nullable()->default(0)->comment('If this account is allowed to access the Admin area.');
			$table->boolean('enabled')->nullable()->default(1)->comment('If this account is allowed to access the comum (content) area.');
			$table->string('avatar', 500)->nullable()->comment('Picture of profile');
			$table->timestamps();
			$table->softDeletes();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
